<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AdmHirearchy extends Model
{
    // Definition
        protected $table    =   "adm_hirearchy";

        
    // Join
        public function roleRel ()
        {
            return $this->belongsTo(AdmRole::class, 'role','id');
        }

        public function processRel ()
        {
            return $this->belongsTo(AdmProcess::class, 'process','id');
        }
        
}
