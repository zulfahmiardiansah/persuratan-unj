<!doctype html>
<html lang="en" dir="ltr">
<head>
    <title>Surat UNJ | Registrasi</title>
    @include('include/asset')
</head>
<body>
    <style>
        .placeholder-body-img {
            position: fixed;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            z-index: -100;
            opacity: .030;
            max-width: 100%;
            cursor: default;
        }
    </style>
    <img src="{{ asset('./assets/images/brand/unj.png') }}" class="placeholder-body-img">
    <div class="page">
        <div class="page-single">
            <div class="container">
                <div class="row">
                    <div class="col col-login mx-auto">
                        <form class="card" action="" method="post">
                            {{ csrf_field() }}
                            <div class="card-body p-6">
                                <div class="card-title"><b>Registrasi</b> Akun</div>
                                <div class="form-group mb-3">
                                    <label>
                                        NID / NIDN <sup class="mandatory-sup">*</sup>
                                    </label>
                                    <input name="nid" type="text" class="form-control" required="">
                                </div>
                                <div class="form-group mb-3">
                                    <label>
                                        Nama <sup class="mandatory-sup">*</sup>
                                    </label>
                                    <input name="name" type="text" class="form-control" required="">
                                </div>
                                <div class="form-group mb-3">
                                    <label>
                                        Email
                                    </label>
                                    <input name="email" type="email" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>
                                        Kata Sandi <sup class="mandatory-sup">*</sup>
                                    </label>
                                    <input name="password" type="password" class="form-control" required="">
                                </div>
                                <div class="form-footer mt-4">
                                    <button type="submit" class="btn btn-info btn-block">Kirim</button>
                                </div>
                            </div>
                        </form>
                        <div class="text-center text-muted">
                            <span>LPPM Universitas Negeri Jakarta</span> @ <b style="color: #00923f">{{ date("Y") }}</b>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(Session::has('error'))
        <script>
            toastr["error"]("{{ Session::get('error') }}", "Kesalahan")
        </script>
    @endif
    @if(Session::has('success'))
        <script>
            toastr["success"]("{{ Session::get('success') }}", "Pemberitahuan")
        </script>
    @endif
</body>
</html>