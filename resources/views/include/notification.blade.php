<a class="nav-link icon" data-toggle="dropdown">
    <i class="fe fe-bell"></i>
    @if (isset($notificationList))
        @if (count($notificationList))
        <span class="nav-unread"></span>
        @endif
    @endif
</a>
@if (isset($notificationList))
    @if (count($notificationList))
        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
            @foreach ($notificationList as $notification)
            <div class="dropdown-item d-flex" style="white-space: normal;width: 435px;max-width: 100%; cursor: pointer;" onclick="$($(this).find('form')).submit()">
                <div>
                    <span style="font-weight: normal;color: #969aa4;">Perkara <strong style="color: #6e7687;">{{ $notification->case }}</strong>
                        menunggu <strong style="color: #6e7687;">{{ $notification->processRel->name }}</strong></span>
                    <div class="small text-muted mt-1"><i class="fe fe-clock"></i>&nbsp; {{ date('d M Y, H:i:s', strtotime($notification->updated_at)) }}</div>
                </div>
                @if ($notification->report_final)
                    <form action="{{ route( $notification->proc_type == 1 ? 'bkd' : 'bka' ) }}" method="POST" style="display: none">
                @else
                    <form action="{{ url('') . '/' . $notification->processRel->url }}" method="POST" style="display: none">
                    <input type="hidden" name="submitType" value="view">
                @endif
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ $notification->id }}">
                    <input type="hidden" name="submitType" value="view">
                </form>
            </div>
            @endforeach
        </div>
    @endif
@endif