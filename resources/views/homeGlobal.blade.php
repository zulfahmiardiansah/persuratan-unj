@include('include/header')

<div class="my-3 my-md-5">
    <div class="container">
        <div class="page-header">
            <h1 class="page-title">
                Informasi Umum
            </h1>
        </div>
        <div class="row row-cards">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="w-100 py-2">
                            <form method="GET" action="">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <h3 class="card-title py-2">
                                            {{ isset($_GET['keyword']) ? 'Hasil pencarian' : 'Seluruh data' }}
                                        </h3>
                                    </div>
                                    <div class="col-lg-2 col-12 text-right">
                                    </div>
                                    <div class="col-lg-2 col-12">
                                        <div class="input-icon my-3 my-lg-0">
                                            <input type="text"
                                                value="{{ isset($_GET['from']) ? $_GET['from'] : '' }}"
                                                name="from" class="form-control header-search"
                                                placeholder="Dari" onfocus="(this.type='date')" onblur="(this.type='text')" tabindex="1">
                                            <div class="input-icon-addon">
                                                <i class="fe fe-calendar"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-12">
                                        <div class="input-icon my-3 my-lg-0">
                                            <input type="text"
                                                value="{{ isset($_GET['to']) ? $_GET['to'] : '' }}"
                                                name="to" class="form-control header-search"
                                                placeholder="Hingga" onfocus="(this.type='date')" onblur="(this.type='text')" tabindex="1">
                                            <div class="input-icon-addon">
                                                <i class="fe fe-calendar"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-12">
                                        <div class="input-icon my-3 my-lg-0">
                                            <input type="search"
                                                value="{{ isset($_GET['keyword']) ? $_GET['keyword'] : '' }}"
                                                name="keyword" class="form-control header-search"
                                                placeholder="Kata Kunci" tabindex="1">
                                            <div class="input-icon-addon">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-1 col-12">
                                        <div class="input-icon my-3 my-lg-0">
                                            <button class="btn btn-secondary btn-sm" style="height: 38px">
                                                <i class="fe fe-search"></i> Cari
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table card-table table-hover table-vcenter text-nowrap">
                            <thead>
                                <tr>
                                    <th class="w-1">No. Surat</th>
                                    <th style="max-width: 250px;">Tanggal Pengajuan</th>
                                    <th>Jenis</th>
                                    <th>Judul</th>
                                    <th>Progress Penyelesaian</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($listProcHeader as $idx => $procHeader)
                                <tr>
                                    <td>
                                        <span class="text-muted">{{ $procHeader->number_report ? $procHeader->number_report : 'Belum memiliki nomor' }}</span>
                                    </td>
                                    <td>
                                        <a class="text-inherit">
                                            {{ date('d M Y', strtotime($procHeader->created_at)) }}
                                            <span class="d-md-inline-block d-none">
                                                , {{ date('H:i:s', strtotime($procHeader->created_at)) }}
                                            </span>
                                        </a>
                                    </td>
                                    <td style="max-width: 250px; white-space: normal;">
                                        {{ $procHeader->type == 1 ? 'Pengabdian' : 'Penelitian' }}
                                    </td>
                                    <td style="max-width: 250px; white-space: normal;">
                                        {{ $procHeader->title }}
                                    </td>
                                    <td style="display: flex; align-items: center;">
                                        @if ($procHeader->status == 1) 
                                            <span class="status-icon bg-success mr-3"></span>
                                        @else
                                            <span class="status-icon bg-danger mr-3"></span>
                                        @endif
                                        <div class="d-inline-block">
                                            {{ $procHeader->processRel->name }}
                                            <div class="small text-muted">
                                                {{ $procHeader->currentRel->name }}
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                @if (count($listProcHeader) == 0) 
                                    <tr>
                                        <td colspan="7">
                                            Tidak ada data
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="table-pagination py-3">
                        {{ $listProcHeader->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('include/footer')

<script>
    $('a[data-url-title="home"]').addClass("active");
</script>