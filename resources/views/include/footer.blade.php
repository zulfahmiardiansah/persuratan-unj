        </div>
        <footer class="footer">
            <div class="container">
                <div class="row align-items-center flex-row-reverse">
                    <div class="col-auto ml-lg-auto d-lg-block d-none">
                        <div class="row align-items-center">
                            <div class="col-auto">
                                <ul class="list-inline list-inline-dots mb-0">
                                    <li class="list-inline-item"><a href="http://lppm.unj.ac.id/">LPPM Universitas Negeri Jakarta</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-auto mt-lg-0 text-center">
                        Kunjungi <a href="http://lppm.unj.ac.id/"><strong style="font-weight: 600;">Situs Resmi</strong></a>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <script>
        startTimeClock();
    </script>
    @if(Session::has('error'))
        <script>
            toastr["error"]("{{ Session::get('error') }}", "Kesalahan")
        </script>
    @endif
    @if(Session::has('success'))
        <script>
            toastr["success"]("{{ Session::get('success') }}", "Pemberitahuan")
        </script>
    @endif
    @if(isset($auth))
        <script>
            setTimeout(() => {
                window.location.replace("{{ route('logout') }}");
            }, 30 * 60 * 1000);
        </script>
    @endif
</body>
</html>