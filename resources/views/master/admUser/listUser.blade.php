@include('include/header')

<div class="my-3 my-md-5">

    <div class="container">
        <div class="page-header">
            <h1 class="page-title">
                Pengguna
            </h1>
        </div>
        <div class="row row-cards">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <form action="" method="POST">
                            {{ csrf_field() }}
                            <button name="submitType" value="view" class="btn btn-sm btn-success">
                                <i class="fe fe-plus"></i> Tambah Data
                            </button>
                        </form>
                    </div>
                    <div class="table-responsive">
                        <table class="table card-table table-hover table-vcenter text-nowrap">
                            <thead>
                                <tr>
                                    <th class="w-4">No.</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>NID / NIDN</th>
                                    <th>Role</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($listAdmUser as $idx => $admUser)
                                <tr>
                                    <td>
                                        <span class="text-muted text-center">
                                            {{ $idx + 1 }}
                                        </span>
                                    </td>
                                    <td>
                                        {{ $admUser->name }}
                                    </td>
                                    <td>
                                        {{ $admUser->email }}
                                    </td>
                                    <td>
                                        {{ $admUser->nid }}
                                    </td>
                                    <td>
                                        {{ $admUser->roleRel->name }}
                                    </td>
                                    <td>
                                        <form action="" method="POST">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="id" value="{{ $admUser->id }}">
                                            <button name="submitType" value="view" class="icon">
                                                <i class="fe fe-edit"></i>
                                            </button>
                                            @if ($idx != 0 && $admUser->roleRel->code != 'ROLE_PEMIMPIN')
                                                &nbsp;
                                                <button name="submitType" value="delete" class="icon">
                                                    <i class="fe fe-trash"></i>
                                                </button>
                                            @endif
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('include/footer')

<script>
    $('a[data-url-title="master"]').addClass("active");
</script>