@include('include/header')

<div class="my-3 my-md-5">
    <div class="container">
        <div class="page-header">
            <h1 class="page-title">
                {{ $siteTitle }}
            </h1>
        </div>
        <form action="{{ route('proc/form') }}" method="POST" enctype="multipart/form-data"> 
            {{ csrf_field() }}
            <div class="row row-cards">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                Form Data
                            </h3>
                        </div>
                        <div class="card-body">
                            <div class="row mb-3">
                                <div class="col-lg-3 col-12 custom-label">
                                    <label>
                                        Jenis <sup class="mandatory-sup">*</sup>
                                    </label>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <select name="type" class="form-control" required="">
                                        <option value="">
                                            - Pilih Jenis -
                                        </option>
                                        <option value="1">
                                            Pengabdian
                                        </option>
                                        <option value="2">
                                            Penelitian
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-lg-3 col-12 custom-label">
                                    <label>
                                        Judul Penelitian/Pengabdian <sup class="mandatory-sup">*</sup>
                                    </label>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <input name="title" type="text" class="form-control" required="">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-lg-3 col-12 custom-label">
                                    <label>
                                        Perihal <sup class="mandatory-sup">*</sup>
                                    </label>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <input name="purpose" type="text" class="form-control" required="">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-lg-3 col-12 custom-label">
                                    <label>
                                        Tanggal Awal Pelaksanaan <sup class="mandatory-sup">*</sup>
                                    </label>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <input name="start_date" type="date" class="form-control" required="">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-lg-3 col-12 custom-label">
                                    <label>
                                        Tanggal Selesai Pelaksanaan <sup class="mandatory-sup">*</sup>
                                    </label>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <input name="end_date" type="date" class="form-control" required="">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-lg-3 col-12 custom-label">
                                    <label>
                                        Tujuan Surat <sup class="mandatory-sup">*</sup>
                                    </label>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <input name="subject[]" type="text" class="form-control" required="">
                                    <div id="subject-form" class="mb-2"></div>
                                    <script>
                                        var subjectHtml   = `<div class="row gutters-xs mt-2">
                                                                <div class="col">
                                                                    <input name="subject[]" type="text" class="form-control">
                                                                </div>
                                                                <span class="col-auto">
                                                                    <button class="btn btn-danger" type="button" onClick="$(this).parent().parent().remove()"><i class="fe fe-trash"></i></button>
                                                                </span>
                                                            </div>`;
                                    </script>
                                    <button type="button" class="btn btn-sm btn-primary" onclick="$('#subject-form').append(subjectHtml)">
                                        <i class="fe fe-plus"></i>&nbsp; Tambah
                                    </button>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-lg-3 col-12 custom-label">
                                    <label>
                                        Isi Surat
                                    </label>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <input name="content_doc_letter" type="file" class="form-control">
                                    <span class="form-hint text-muted">
                                        * Draf Isi Surat Bentuk .doc atau .docx (jika ada)
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3 col-12 custom-label">
                                    <label>
                                        Lampiran Surat
                                    </label>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <input name="attachment_doc_letter" type="file" class="form-control">
                                    <span class="form-hint text-muted">
                                        * Lampiran Surat bentuk .doc atau .docx (jika ada)
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                Catatan / Komentar
                            </h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-2 col-12 custom-label">
                                    <label>
                                        Catatan
                                    </label>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <textarea name="response" spellcheck="false" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <a href="{{ route('home') }}">
                        <button type="button" class="btn btn-md btn-secondary d-inline-block">
                            <i class="fe fe-arrow-left"></i> Kembali
                        </button>
                    </a>
                </div>
                <div class="col-6 text-right">
                    <button name="submitType" value="process" class="btn btn-md btn-success d-inline-block">
                        <i class="fe fe-save"></i> Simpan
                    </button>
                </div>
            </div>
        </form>
        <br>
    </div>
</div>

@include('include/footer')

<script>
    $('a[data-url-title="formulir"]').addClass("active");
    $('form').submit(function(e){
        $('button').attr("type", "button")
    });
</script>
