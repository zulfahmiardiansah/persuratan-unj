<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\AdmUser;
use App\Model\AdmRole;
use App\Model\AdmProcess;
use App\Model\AdmHirearchy;

use Hash, Redirect;

class MasterController extends Controller
{
    // Model
        private $model  =   [];

    // Ready Model
        function readyModel () {
            $this->model    =   Utility::getSessionScope();
        }

    // Adm User Page
        public function admUserPage (Request $request)
        {
            $this->readyModel();
            $this->model['siteTitle']   =   'Manajemen Aplikasi';
            $this->model['listAdmUser'] =   AdmUser::orderBy('id', 'asc')->get();
            return view('master.admUser.listUser', $this->model);
        }

        public function admUserProc (Request $request) {
            $this->readyModel();
            if ($request->get('submitType') == 'view') {
                $this->model['siteTitle']   =   'Manajemen Aplikasi';
                if ($request->get('id')) {
                    $this->model['admUser'] =   AdmUser::find($request->get('id'));
                } else {
                    $this->model['admUser'] =   new AdmUser();
                }
                $this->model['listAdmRole'] =   AdmRole::orderBy('id', 'ASC')->get();
                return view('master.admUser.formUser', $this->model);
            } else if ($request->get('submitType') == 'save') {
                if ($request->get('id')) {
                    $admUser    =   AdmUser::find($request->get('id'));
                } else {
                    $admUser    =   new AdmUser();
                }
                $admUser->name      =   $request->get('name');
                $admUser->nid       =   $request->get('nid');
                $admUser->email     =   $request->get('email');
                if ($request->get('password')) {
                    $admUser->password  =   Hash::make($request->get('password'));
                }
                if ($request->file("sign")) {
                    $admUser->sign  =   Utility::uploadFile($request, "sign");
                }
                if ($request->get('role')) {
                    $admUser->role      =   $request->get('role');
                }
                $admUser->save();
                return Redirect::to(route('master/admUser'))->with('success', 'Data berhasil disimpan');
            } else if ($request->get('submitType') == 'delete') {
                AdmUser::find($request->get('id'))->delete();
                return Redirect::to(route('master/admUser'))->with('success', 'Data berhasil dihapus');
            }
        }

    // Adm Role Page
        public function admRolePage (Request $request)
        {
            $this->readyModel();
            $this->model['siteTitle']   =   'Manajemen Aplikasi';
            $this->model['listAdmRole'] =   AdmRole::orderBy('id', 'asc')->get();
            return view('master.admRole.listRole', $this->model);
        }

        public function admRoleProc (Request $request) {
            $this->readyModel();
            if ($request->get('submitType') == 'view') {
                $this->model['siteTitle']   =   'Manajemen Aplikasi';
                if ($request->get('id')) {
                    $this->model['admRole'] =   AdmRole::find($request->get('id'));
                } else {
                    $this->model['admRole'] =   new AdmRole();
                }
                return view('master.admRole.formRole', $this->model);
            } else if ($request->get('submitType') == 'save') {
                if ($request->get('id')) {
                    $admRole    =   AdmRole::find($request->get('id'));
                } else {
                    $admRole    =   new AdmRole();
                }
                $admRole->name      =   $request->get('name');
                $admRole->code      =   $request->get('code');
                $admRole->save();
                return Redirect::to(route('master/admRole'))->with('success', 'Data berhasil disimpan');
            } else if ($request->get('submitType') == 'delete') {
                AdmRole::find($request->get('id'))->delete();
                return Redirect::to(route('master/admRole'))->with('success', 'Data berhasil dihapus');
            }
        }

    // Adm Hirearchy Page
        public function admHirearchyPage (Request $request)
        {
            $this->readyModel();
            $this->model['siteTitle']   =   'Manajemen Aplikasi';
            $this->model['listAdmProcess']  =   AdmProcess::get();
            $this->model['listAdmRole']     =   AdmRole::get();
            $this->model['listAdmHirearchyBKA']    =   AdmHirearchy::orderBy('sequence', 'ASC')->get();
            return view('master.admHirearchy.listHirearchy', $this->model);
        }

        public function admHirearchyProc (Request $request) {
            $this->readyModel();
            if ($request->get('submitType') == 'save') {
                $admHirearchy    =   new AdmHirearchy();
                $admHirearchy->role     =   $request->get('role');
                $admHirearchy->process  =   $request->get('process');
                $admHirearchy->sequence   =   $request->get('sequence');
                $admHirearchy->save();
                return Redirect::to(route('master/admHirearchy'))->with('success', 'Data berhasil disimpan');
            } else if ($request->get('submitType') == 'delete') {
                AdmHirearchy::find($request->get('id'))->delete();
                return Redirect::to(route('master/admHirearchy'))->with('success', 'Data berhasil dihapus');
            }
        }
}
