<table>
    <thead>
        <tr>
            <td><b>No. Permintaan</b></td>
            <td><b>Tanggal Permintaan</b></td>
            <td><b>Tanggal Pelaksanaan</b></td>
            <td><b>Tanggal Penyelesaian</b></td>
            <td><b>Asal Permintaan</b></td>
            <td><b>Nama Klien</b></td>
            <td><b>Perkara</b></td>
            <td><b>Pembimbing Kemasyarakatan</b></td>
            <td><b>Progress Penyelesaian</b></td>
            <td><b>Petugas Saat Ini</b></td>
        </tr>
    </thead>
    <tbody>
        @foreach ($listProcHeader as $idx => $procHeader)
        <tr>
            <td>{{ $procHeader->letter_number }}</td>
            <td>{{ date('d M Y, H:i:s', strtotime($procHeader->created_at)) }}</td>
            <td>{{ $procHeader->start ? date("d M Y", strtotime($procHeader->start)) : '' }}</td>
            <td>{{ $procHeader->end ? date("d M Y", strtotime($procHeader->end)) : '' }}</td>
            <td>{{ $procHeader->origin }}</td>
            <td>{{ $procHeader->initiator_name }}</td>
            <td>{{ $procHeader->case }}</td>
            <td>{{ $procHeader->worker_name }}</td>
            <td>{{ $procHeader->report_final ? "Selesai" : $procHeader->processRel->name }}</td>
            <td>{{ $procHeader->report_final ? "" : $procHeader->currentRel->name }}</td>
        </tr>
        @endforeach
    </tbody>
</table>