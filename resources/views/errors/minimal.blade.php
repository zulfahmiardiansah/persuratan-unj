<!doctype html>
<html lang="en" dir="ltr">
<head>
    <title>SILAMABAS | @yield('code')</title>
    @include('include/asset')
</head>
<body>
    <div class="page">
        <div class="page-content">
            <div class="container text-center">
                <div class="display-1 text-muted mb-5"><i class="si si-exclamation"></i> @yield('code')</div>
                <h1 class="h2 mb-3"> @yield('message')</h1>
            </div>
        </div>
    </div>
</body>
</html>
