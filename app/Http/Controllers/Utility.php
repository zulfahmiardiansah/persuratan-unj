<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\AdmRole;
use App\Model\ProcPreAdj;
use App\Model\SesScope;
use App\Model\ProcHeader;

class Utility extends Controller
{
    // Get Session Data
        public static function getSessionScope ()
        {
            $result     =   [];
            if (isset($_COOKIE['611ce1959e28922b42df3474f95f4a1d'])) {
                $sesScope           =   SesScope::where('key', $_COOKIE['611ce1959e28922b42df3474f95f4a1d'])->first();
                if ($sesScope) {
                    $result['auth']     =   json_decode($sesScope->value);
                }
            }
            return $result;
        }

    // Upload File
        public static function uploadFile (Request $request, string $name, $customFolder = 'media/', $customName = false)
        {
            $file = $request->file($name);
            $newName = null;
            if ($file != false) {
                $storageUrl =   "storage/files/" .  $customFolder . date("Y") . "-" . date("m") . "-" . date("d") . "/";
                $newName    =  date("His") . "_" . ($customName ? $customName . '.' . $file->getClientOriginalExtension() : $file->getClientOriginalName());
                $file->move($storageUrl, $newName);
                $newName    = $storageUrl . $newName;
            }
            return $newName;
        }

    // Base 64 Upload
        public function base64Upload (Request $request) {
            $fileUrlArray = [];
            $imageContainer = $request->get("imageContainer");

            foreach ($imageContainer as $base64) {
                $fileUrl = "files/doc-report-pra-final/" . date("dmY-His-") . rand(11111,99999) . ".jpg";
                \Storage::disk("public_root")->put( "storage/" . $fileUrl, base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $base64)));
                $fileUrlArray[] = $fileUrl;
            }
            
            $pdf = new \setasign\Fpdi\Fpdi();
            foreach ($fileUrlArray as $fileUrl) {
                list($width, $height, $type, $attr) = getimagesize(public_path("storage/" . $fileUrl));
                $pdf->SetSize($width / 1.3, $height / 1.3);
                $pdf->AddPage('', 'custom');
                $pdf->Image(public_path("storage/" . $fileUrl), 0, 0, 0, 0);
            }

            $path = "storage/files/doc-report-final/" . date("Y") . "-" . date("m") . "-" . date("d") . "/";
            $fileName = $path . date("His") . "_REPORT_FINAL_" . $request->get("id") . ".pdf";
            try { \File::makeDirectory($path); } catch (\Throwable $exception) {}
            $pathFile = public_path($fileName);
            $pdf->Output($pathFile, "F");
            foreach ($fileUrlArray as $fileUrl) {
                \Storage::delete("public/" . $fileUrl);
            }

            return response(["fileUrl" => asset($fileName), "file" => $fileName]);
        }

}
