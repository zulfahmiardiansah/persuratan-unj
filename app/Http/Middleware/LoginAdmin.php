<?php

namespace App\Http\Middleware;

use Closure, Redirect;
use App\Http\Controllers\Utility;

class LoginAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $sesScope = Utility::getSessionScope();
        if ($sesScope) {
            if ($sesScope['auth']->roleCode == 'ROLE_ADMIN') {
                return $next($request);
            } else {
                return Redirect::to(route('home'))->with("error", "Anda tidak memiliki akses untuk menu tersebut");
            }
        } else {
            return Redirect::to(route('login'))->with("error", "Anda harus masuk untuk melanjutkan");
        }
    }
}
