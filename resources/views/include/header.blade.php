<!doctype html>
<html lang="en" dir="ltr">

<head>
    <title>Surat UNJ | {{ isset($siteTitle) ? $siteTitle : '' }}</title>
    @include('include/asset')
    <style>
        .custom-label label {
            color: #999;
        }
        .placeholder-body-img {
            position: fixed;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            z-index: -100;
            opacity: .030;
            max-width: 100%;
            cursor: default;
        }
        .blink-on {
            color: #458a78;
        }
        .blink-off {
            color: #ea820f;
        }
        .card-header {
            border-top: 2px solid #00923f;
        }
    </style>
</head>

<body class="">
    <img src="{{ asset('./assets/images/brand/unj.png') }}" class="placeholder-body-img">
    <div class="page">
        <div class="page-main">
            <div class="header py-3">
                <div class="container">
                    <div class="d-flex">
                        <a class="header-brand" href="#">
                            <!-- <img src="{{ asset('./assets/images/brand/unj.png') }}" class="header-brand-img"
                                alt="tabler logo"> -->
                            <!-- <img src="{{ asset('./assets/images/brand/surat-unj.png') }}" class="header-brand-img"
                                alt="tabler logo"> -->
                            <img src="{{ asset('./assets/images/brand/surat-unj-monogram.png') }}" class="header-brand-img"
                                alt="tabler logo" style="height: 2.5rem;">
                        </a>
                        <div class="d-flex order-lg-2 ml-auto">
                            @if (isset($auth))
                            <div class="dropdown d-none d-md-flex" id="notificationContainer">
                                <a class="nav-link icon" data-toggle="dropdown">
                                    <i class="fe fe-bell"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                </div>
                            </div>
                            @if ($auth->roleCode != 'ROLE_USER')
                            <script>
                                $("#notificationContainer").load("{{ route('notification') }}");
                                setInterval(() => {
                                    $("#notificationContainer").load("{{ route('notification') }}");
                                }, 10 * 60 * 1000);

                            </script>
                            @endif
                            <div class="dropdown">
                                <a href="#" class="nav-link pr-0 leading-none" data-toggle="dropdown">
                                    <!-- <span class="avatar" style="background-image: url({{ asset('./assets/images/faces/default.png') }})"></span> -->
                                    <span class="avatar" style="background-image: url({{ asset('./assets/images/brand/unj.png') }})"></span>
                                    <span class="ml-3 d-none d-lg-block">
                                        <span class="text-default">{{ $auth->name }}</span>
                                        <small class="text-muted d-block mt-1">{{ $auth->roleName }}</small>
                                    </span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                    <a class="dropdown-item" href="{{ route('profile') }}">
                                        <i class="dropdown-icon fe fe-user"></i> Profil
                                    </a>
                                    <a class="dropdown-item" href="{{ route('logout') }}">
                                        <i class="dropdown-icon fe fe-log-out"></i> Keluar
                                    </a>
                                </div>
                            </div>
                            @else
                            <div class="nav-item d-flex p-0 pr-3">
                                <a href="{{ route('registration') }}" class="btn btn-sm btn-outline-info">
                                    <i class="fe fe-user-plus"></i>&nbsp; Registrasi
                                </a>
                            </div>
                            <div class="nav-item d-flex p-0">
                                <a href="{{ route('login') }}" class="btn btn-sm btn-success">
                                    <i class="fe fe-log-in"></i>&nbsp; Masuk
                                </a>
                            </div>
                            @endif
                        </div>
                        <a href="#" class="header-toggler d-lg-none ml-3 ml-lg-0" data-toggle="collapse"
                            data-target="#headerMenuCollapse">
                            <span class="header-toggler-icon"></span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="header collapse d-lg-flex p-0" id="headerMenuCollapse">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-3 ml-auto d-none d-lg-block">
                            <div class="my-3 my-lg-0 text-right text-muted" style="font-weight: 400;font-size: .92em;">
                                <i class="fe fe-clock d-inline-block" style="transform: translateY(1px);"></i>&nbsp;
                                Pukul <span id="clock" style="font-weight: 600">00:00:00</span>
                            </div>
                        </div>
                        <div class="col-lg order-lg-first">
                            <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                                <li class="nav-item">
                                    <a href="{{ route('home') }}" class="nav-link" data-url-title="home"><i
                                            class="fe fe-home"></i> Beranda</a>
                                </li>
                                @if (isset($auth))
                                @if ($auth->roleCode == 'ROLE_USER')
                                <li class="nav-item">
                                    <a href="{{ route('proc/form') }}" class="nav-link" data-url-title="formulir"><i
                                            class="fe fe-clipboard"></i>
                                        Formulir</a>
                                </li>
                                @endif
                                @endif
                                <li class="nav-item">
                                    <a href="{{ route('devotion') }}" class="nav-link" data-url-title="pengabdian"><i
                                            class="fe fe-users"></i>
                                        Pengabdian</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('research') }}" class="nav-link" data-url-title="penelitian"><i
                                            class="fe fe-search"></i>
                                        Penelitian</a>
                                </li>
                                @if (isset($auth))
                                @if ($auth->roleCode == 'ROLE_ADMIN')
                                <li class="nav-item">
                                    <a href="javascript:void(0)" class="nav-link" data-toggle="dropdown"
                                        data-url-title="master"><i class="fe fe-settings"></i>
                                        Manajemen Aplikasi</a>
                                    <div class="dropdown-menu dropdown-menu-arrow">
                                        <a href="{{ route('master/admUser') }}" class="dropdown-item ">Pengguna</a>
                                        <!-- <a href="{{ route('master/admHirearchy') }}" class="dropdown-item ">Hirarki
                                            Proses</a> -->
                                    </div>
                                </li>
                                @endif
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
