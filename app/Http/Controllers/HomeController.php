<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\ProcHeader;
use App\Model\AdmRole;

class HomeController extends Controller
{
    // Model
        private $model  =   [];

    // Ready Model
        function readyModel () {
            $this->model    =   Utility::getSessionScope();
        }

    // Home Page
        public function homePage (Request $request) {
            $this->readyModel();
            $this->model['siteTitle']   =   'Beranda';
            if (isset($this->model['auth'])) {
                $admRole    =   AdmRole::where("code", $this->model['auth']->roleCode)->first();
                if ($admRole->code == 'ROLE_USER') {
                    $this->model['listProcHeader']   =   ProcHeader::whereNull("is_delete")->where("current", $admRole->id)->where('initiator', $this->model['auth']->id)->orderBy("created_at", "DESC")->get();
                } else {
                    $listProcHeader   =   ProcHeader::whereNull("is_delete")->where("current", $admRole->id)->orderBy("created_at", "DESC")->get();
                    $result     =   [];
                    foreach ($listProcHeader as $procHeader) {
                        if (($procHeader->process == 2 || $procHeader->process == 4) && $procHeader->operator) {
                            if ($procHeader->operator == $this->model['auth']->id) {
                                $result[]   =   $procHeader;
                            }
                        } else {
                            $result[]   =   $procHeader;
                        }
                    }
                    $listProcHeader =   $result;
                    $this->model['listProcHeader']   =   $listProcHeader;
                }
                return view("home", $this->model);
            } else {
                $getPath    =   "";
                $pathArray  =   [];
                $this->model['listProcHeader']  =   ProcHeader::whereNull("is_delete")->orderBy("created_at", "DESC");
                if ($request->get("keyword")) {
                    $this->model['listProcHeader']  =   $this->model['listProcHeader']->where(function ($q) use ($request) {
                                                                $q->where("title", "LIKE", "%" . $request->get("keyword") . "%")
                                                                ->orWhere("purpose", "LIKE", "%" . $request->get("keyword") . "%")
                                                                ->orWhere("subject", "LIKE", "%" . $request->get("keyword") . "%")
                                                                ->orWhere("number_report", "LIKE", "%" . $request->get("keyword") . "%")
                                                                ->orWhere("initiator_name", "LIKE", "%" . $request->get("keyword") . "%")
                                                                ->orWhere("operator_name", "LIKE", "%" . $request->get("keyword") . "%");
                                                            });
                    $pathArray[]    =   "keyword=" . $request->get("keyword");
                }
                if ($request->get("from"))  {
                    $this->model['listProcHeader']  =   $this->model['listProcHeader']->whereDate('created_at', '>=', $request->get('from'));
                    $pathArray[]    =   "from=" . $request->get("from");
                }
                if ($request->get("to"))  {
                    $this->model['listProcHeader']  =   $this->model['listProcHeader']->whereDate('created_at', '<=', $request->get('to'));
                    $pathArray[]    =   "to=" . $request->get("to");
                }
                if (isset($this->model['auth'])) {
                    if ($this->model['auth']->roleCode  ==  'ROLE_USER') {
                        $this->model['listProcHeader']  =   $this->model['listProcHeader']->where('initiator', $this->model['auth']->id);
                    }
                }
                if (count($pathArray)) {
                    $getPath    =   "?" . implode("&", $pathArray);
                    $this->model["getPath"] =   $getPath;
                }
                $this->model['listProcHeader']  =   $this->model['listProcHeader']->paginate(10)->withPath($getPath);
                return view('homeGlobal', $this->model);
            }
        }

        public function notificationProc () {
            $this->readyModel();
            if (isset($this->model['auth'])) {
                $admRole    =   AdmRole::where("code", $this->model['auth']->roleCode)->first();
                if ($admRole->code  !=  'ROLE_USER') {
                    $notificationList   =   ProcHeader::whereNull("is_delete")->where("current", $admRole->id)->orderBy("created_at", "DESC")->get();
                    $result     =   [];
                    foreach ($notificationList as $procHeader) {
                        if (($procHeader->process == 2 || $procHeader->process == 4) && $procHeader->operator) {
                            if ($procHeader->operator == $this->model['auth']->id) {
                                $result[]   =   $procHeader;
                            }
                        } else {
                            $result[]   =   $procHeader;
                        }
                    }
                    $notificationList =   $result;
                    $this->model['notificationList']    =   $notificationList;
                }
                return view("include/notification", $this->model);
            }
        }
}
