@include('include/header')

<div class="my-3 my-md-5">
    <div class="container">
        <div class="page-header">
            <h1 class="page-title">
                {{ $siteTitle }}
            </h1>
        </div>
        <form action="{{ route('proc/form') }}" method="POST" enctype="multipart/form-data"> 
            {{ csrf_field() }}
            <input type="hidden" name="id" value="{{ $procHeader->id }}">
            <div class="row row-cards">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                Form Data
                            </h3>
                        </div>
                        <div class="card-body">
                            <div class="row mb-3">
                                <div class="col-lg-3 col-12 custom-label">
                                    <label>
                                        Judul <sup class="mandatory-sup">*</sup>
                                    </label>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <input name="title" value="{{ $procHeader->title }}" type="text" class="form-control" required="">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-lg-3 col-12 custom-label">
                                    <label>
                                        Perihal <sup class="mandatory-sup">*</sup>
                                    </label>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <input name="purpose" value="{{ $procHeader->purpose }}" type="text" class="form-control" required="">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-lg-3 col-12 custom-label">
                                    <label>
                                        Tanggal Awal Pelaksanaan <sup class="mandatory-sup">*</sup>
                                    </label>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <input name="start_date" value="{{ $procHeader->start_date }}" type="date" class="form-control" required="">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-lg-3 col-12 custom-label">
                                    <label>
                                        Tanggal Selesai Pelaksanaan <sup class="mandatory-sup">*</sup>
                                    </label>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <input name="end_date" value="{{ $procHeader->end_date }}" type="date" class="form-control" required="">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-lg-3 col-12 custom-label">
                                    <label>
                                        Jenis <sup class="mandatory-sup">*</sup>
                                    </label>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <select name="type" class="form-control" required="">
                                        @if ($procHeader->type == 1)
                                            <option value="1">
                                                Pengabdian
                                            </option>
                                        @else
                                            <option value="2">
                                                Penelitian
                                            </option>
                                        @endif
                                        <option value="">
                                            - Pilih Jenis -
                                        </option>
                                        <option value="1">
                                            Pengabdian
                                        </option>
                                        <option value="2">
                                            Penelitian
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-lg-3 col-12 custom-label">
                                    <label>
                                        Tujuan Surat <sup class="mandatory-sup">*</sup>
                                    </label>
                                </div>
                                <div class="col-lg-6 col-12">
                                    @if (count(explode('|', $procHeader->subject)))
                                        @foreach (explode('|', $procHeader->subject) as $idx => $item)
                                            @if ($idx == 0)
                                                <input name="subject[]" value="{{ $item }}" type="text" class="form-control" required="">
                                            @else
                                                <div class="row gutters-xs mt-2">
                                                    <div class="col">
                                                        <input name="subject[]" value="{{ $item }}" type="text" class="form-control">
                                                    </div>
                                                    <span class="col-auto">
                                                        <button class="btn btn-danger" type="button" onClick="$(this).parent().parent().remove()"><i class="fe fe-trash"></i></button>
                                                    </span>
                                                </div>
                                            @endif
                                        @endforeach
                                    @else
                                        <input name="subject[]" type="text" class="form-control" required="">
                                    @endif
                                    <div id="subject-form" class="mb-2"></div>
                                    <script>
                                        var subjectHtml   = `<div class="row gutters-xs mt-2">
                                                                <div class="col">
                                                                    <input name="subject[]" type="text" class="form-control">
                                                                </div>
                                                                <span class="col-auto">
                                                                    <button class="btn btn-danger" type="button" onClick="$(this).parent().parent().remove()"><i class="fe fe-trash"></i></button>
                                                                </span>
                                                            </div>`;
                                    </script>
                                    <button type="button" class="btn btn-sm btn-primary" onclick="$('#subject-form').html($('#subject-form').html() + subjectHtml)">
                                        <i class="fe fe-plus"></i>&nbsp; Tambah
                                    </button>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-lg-3 col-12 custom-label">
                                    <label>
                                        Isi Surat
                                    </label>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <div class="row gutters-xs mt-2">
                                        <div class="col">
                                            <input name="content_doc_letter" accept="application/pdf" type="file" class="form-control" id="filePdf" onchange="fileValidationPdf();">
                                        </div>
                                        @if ($procHeader->content_doc_letter)
                                            <span class="col-auto">
                                                <a href="{{ url($procHeader->content_doc_letter) }}" download>
                                                    <button class="btn btn-secondary" type="button">
                                                        <i class="fe fe-download"></i> Berkas Sebelumnya
                                                    </button>
                                                </a>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3 col-12 custom-label">
                                    <label>
                                        Lampiran Surat
                                    </label>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <div class="row gutters-xs mt-2">
                                        <div class="col">
                                            <input name="attachment_doc_letter" accept="application/pdf" type="file" class="form-control" id="filePdf" onchange="fileValidationPdf();">
                                        </div>
                                        @if ($procHeader->attachment_doc_letter)
                                            <span class="col-auto">
                                                <a href="{{ url($procHeader->attachment_doc_letter) }}" download>
                                                    <button class="btn btn-secondary" type="button">
                                                        <i class="fe fe-download"></i> Berkas Sebelumnya
                                                    </button>
                                                </a>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                Catatan
                            </h3>
                        </div>
                        <div class="table-responsive">
                            <table class="table card-table table-hover table-vcenter text-nowrap">
                                <thead>
                                    <tr>
                                        <th class="w-1">No.</th>
                                        <th>Tanggal</th>
                                        <th>Proses</th>
                                        <th>Nama</th>
                                        <th>Role</th>
                                        <th>Catatan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($procHeader->commentRel() as $idx => $procComment)
                                        <tr>
                                            <td>
                                                {{ $idx + 1 }}.
                                            </td>
                                            <td>
                                                {{ date("d M Y, H:i:s", strtotime($procComment->created_at)) }}
                                            </td>
                                            <td>
                                                {{ $procComment->process }}
                                            </td>
                                            <td>
                                                {{ $procComment->name }}
                                            </td>
                                            <td>
                                                {{ $procComment->role }}
                                            </td>
                                            <td>
                                                {{ $procComment->response }}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                Catatan / Komentar
                            </h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-2 col-12 custom-label">
                                    <label>
                                        Catatan
                                    </label>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <textarea name="response" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <a href="{{ route('home') }}">
                        <button type="button" class="btn btn-md btn-secondary d-inline-block">
                            <i class="fe fe-arrow-left"></i> Kembali
                        </button>
                    </a>
                </div>
                <div class="col-6 text-right">
                    <button name="submitType" value="process" class="btn btn-md btn-success d-inline-block">
                        <i class="fe fe-save"></i> Simpan
                    </button>
                </div>
            </div>
        </form>
        <br>
    </div>
</div>

@include('include/footer')

<script>
    $('a[data-url-title="formulir"]').addClass("active");
    $('form').submit(function(e){
        $('button').attr("type", "button")
    });
</script>
