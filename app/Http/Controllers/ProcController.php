<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\ProcHeader;
use App\Model\ProcReport;
use App\Model\ProcComment;
use App\Model\AdmHirearchy;
use App\Model\AdmProcess;
use App\Model\AdmPlace;
use App\Model\AdmUser;

use Redirect;
use PDFMerger;

class ProcController extends Controller
{
    // Model
        private $model  =   [];

    // Ready Model
        function readyModel () {
            $this->model    =   Utility::getSessionScope();
        }
    
    // Sample
        public function sample () {
            $pdf = new \setasign\Fpdi\Fpdi();
            $pageCount  =   $pdf->setSourceFile(public_path("../storage/app/public/template-2.pdf"));
            echo $pageCount;
            $qrImage = file_get_contents("https://quickchart.io/qr?text=sample");
            \Storage::put("public/qr.png", $qrImage);
            for ($i = 1; $i <= $pageCount ; $i++) { 
                $pdf->AddPage();
                $tppl = $pdf->importPage($i);
                $pdf->useTemplate($tppl);
                $pdf->Image(public_path("../storage/app/public/qr.png"), 7, 263, 30, 30);
            }
            $pdf->Output(public_path("out.pdf"), "F");
            
        }

        public function generatePdfQrSign ($finalDocUrl, $qrText, $resultUrl) {
            $pdf = new \setasign\Fpdi\Fpdi();
            $pageCount  =   $pdf->setSourceFile(public_path($finalDocUrl));
            echo $pageCount;
            $qrImage = file_get_contents("https://quickchart.io/qr?text=" . route('scan', $qrText));
            $qrImageName = date("YmdHis") . ".png";
            \Storage::put("public/files/qr/" . $qrImageName, $qrImage);
            for ($i = 1; $i <= $pageCount ; $i++) { 
                $pdf->AddPage();
                $tppl = $pdf->importPage($i);
                $pdf->useTemplate($tppl);
                $pdf->Image(public_path("storage/files/qr/" . $qrImageName), 7, 263, 30, 30);
            }
            $pdf->Output(public_path($resultUrl), "F");
        }

    // Form Page
        public function formPage (Request $request)
        {
            $this->readyModel();
            $this->model['siteTitle']   =   "Pengisian Formulir";
            return view('proc.formRequest', $this->model);
        }

    // Form Save Proc
        public function formSaveProc (Request $request)
        {
            $this->readyModel();
            if ($request->get('submitType') == 'view') {
                $this->model['procHeader']  =   ProcHeader::find($request->get('id'));
                $this->model['siteTitle']   =   'Perbaikan Isi Formulir';
                return view('proc.formRequestRevision', $this->model);
            } else if ($request->get("submitType") == 'process') {
                $admHirearchy   =   AdmHirearchy::where('sequence', 2)->first();
                if (!$admHirearchy) {
                    return Redirect::to(route('home'))->with("error", "Proses tidak bisa dilanjutkan");
                }
                $admProcess         =   AdmProcess::find(1);
                $contentDocUrl      =   Utility::uploadFile($request, "content_doc_letter", "doc-request/");
                $attachmentDocUrl   =   Utility::uploadFile($request, "attachment_doc_letter", "doc-request/");
                if ($request->get("id")) {
                    $procHeader     =   ProcHeader::find($request->get('id'));
                } else {
                    $procHeader         =   new ProcHeader();
                }
                $procHeader->title      =   $request->get('title');
                $procHeader->purpose    =   $request->get('purpose');
                $procHeader->start_date =   $request->get('start_date');
                $procHeader->end_date   =   $request->get('end_date');
                $procHeader->type       =   $request->get('type');
                $procHeader->subject    =   implode("|", $request->get("subject"));
                if (!is_null($contentDocUrl)) {
                    $procHeader->content_doc_letter     =   $contentDocUrl;
                }
                if (!is_null($attachmentDocUrl)) {
                    $procHeader->attachment_doc_letter  =   $attachmentDocUrl;
                }
                $procHeader->initiator          =   $this->model['auth']->id;
                $procHeader->initiator_name     =   $this->model['auth']->name;
                $procHeader->sequence   =   2;
                $procHeader->status     =   1;
                $procHeader->current    =   $admHirearchy->role;
                $procHeader->process    =   $admHirearchy->process;
                $procHeader->save();
                $procComment        =   new ProcComment();
                $procComment->name  =   $this->model['auth']->name;
                $procComment->role  =   $this->model['auth']->roleName;
                $procComment->process   =   $admProcess->name;
                $procComment->response  =   $request->get('response');
                $procComment->proc_id   =   $procHeader->id;
                $procComment->save();
                return Redirect::to(route('home'))->with("success", "Data berhasil disimpan");
            }
            return Redirect::to(route("home"));
        }

    // Form Upload Doc Proc
        public function uploadDocProc (Request $request) {
            $this->readyModel();
            if ($request->get('submitType') == 'view') {
                $this->model['procHeader']  =   ProcHeader::find($request->get('id'));
                $this->model['siteTitle']   =   'Pemeriksaan Formulir dan Unggah Draf Surat';
                return view('proc.formUploadDoc', $this->model);
            } else if ($request->get("submitType") == 'process') {
                $procHeader     =   ProcHeader::find($request->get('id'));
                $admHirearchy   =   AdmHirearchy::where('sequence', ($procHeader->sequence + 1))
                                                    ->first();
                $admProcess     =   AdmProcess::find($procHeader->process);
                if (!$admHirearchy) {
                    return Redirect::to(route('home'))->with("error", "Proses tidak bisa dilanjutkan");
                }
                $reportDocUrl   =   Utility::uploadFile($request, "doc_report", "doc-report/");
                if (!is_null($reportDocUrl)) {
                    $procHeader->doc_report =   $reportDocUrl;
                }
                $procHeader->current    =   $admHirearchy->role;
                $procHeader->process    =   $admHirearchy->process;
                $procHeader->sequence   =   $procHeader->sequence + 1;
                $procHeader->operator          =   $this->model['auth']->id;
                $procHeader->operator_name     =   $this->model['auth']->name;
                $procHeader->save();
                $procComment        =   new ProcComment();
                $procComment->name  =   $this->model['auth']->name;
                $procComment->role  =   $this->model['auth']->roleName;
                $procComment->process   =   $admProcess->name;
                $procComment->response  =   $request->get('response');
                $procComment->proc_id   =   $procHeader->id;
                $procComment->save();
                return Redirect::to(route('home'))->with("success", "Data berhasil disimpan");
            } else if ($request->get("submitType") == 'revision') {
                $procHeader     =   ProcHeader::find($request->get('id'));
                $admHirearchy   =   AdmHirearchy::where('sequence', ($procHeader->sequence - 1))
                                                    ->first();
                $admProcess     =   AdmProcess::find($procHeader->process);
                if (!$admHirearchy) {
                    return Redirect::to(route('home'))->with("error", "Proses tidak bisa dilanjutkan");
                }
                $procHeader->current    =   $admHirearchy->role;
                $procHeader->process    =   $admHirearchy->process;
                $procHeader->sequence   =   $procHeader->sequence - 1;
                $procHeader->operator          =   $this->model['auth']->id;
                $procHeader->operator_name     =   $this->model['auth']->name;
                $procHeader->save();
                $procComment        =   new ProcComment();
                $procComment->name  =   $this->model['auth']->name;
                $procComment->role  =   $this->model['auth']->roleName;
                $procComment->process   =   $admProcess->name;
                $procComment->response  =   $request->get('response');
                $procComment->proc_id   =   $procHeader->id;
                $procComment->save();
                return Redirect::to(route('home'))->with("success", "Data berhasil disimpan");
            } else if ($request->get("submitType") == 'reject') {
                $procHeader     =   ProcHeader::find($request->get('id'));
                $admHirearchy   =   AdmHirearchy::where('sequence', (1))
                                                    ->first();
                $admProcess     =   AdmProcess::find($procHeader->process);
                if (!$admHirearchy) {
                    return Redirect::to(route('home'))->with("error", "Proses tidak bisa dilanjutkan");
                }
                $procHeader->current    =   $admHirearchy->role;
                $procHeader->process    =   6;
                $procHeader->sequence   =   0;
                $procHeader->status     =   0;
                $procHeader->operator          =   $this->model['auth']->id;
                $procHeader->operator_name     =   $this->model['auth']->name;
                $procHeader->save();
                $procComment        =   new ProcComment();
                $procComment->name  =   $this->model['auth']->name;
                $procComment->role  =   $this->model['auth']->roleName;
                $procComment->process   =   $admProcess->name;
                $procComment->response  =   $request->get('response');
                $procComment->proc_id   =   $procHeader->id;
                $procComment->save();
                return Redirect::to(route('home'))->with("success", "Data berhasil disimpan");
            }
            return Redirect::to(route("home"));
        }
    
    // Form Approval Proc
        public function approvalProc (Request $request) {
            $this->readyModel();
            if ($request->get('submitType') == 'view') {
                $this->model['procHeader']  =   ProcHeader::find($request->get('id'));
                $this->model['siteTitle']   =   'Pemeriksaan dan Persetujuan Pimpinan';
                return view('proc.formApproval', $this->model);
            } else if ($request->get("submitType") == 'process') {
                $procHeader     =   ProcHeader::find($request->get('id'));
                $admHirearchy   =   AdmHirearchy::where('sequence', ($procHeader->sequence + 1))
                                                    ->first();
                $admProcess     =   AdmProcess::find($procHeader->process);
                if (!$admHirearchy) {
                    return Redirect::to(route('home'))->with("error", "Proses tidak bisa dilanjutkan");
                }
                $procHeader->current    =   $admHirearchy->role;
                $procHeader->process    =   $admHirearchy->process;
                $procHeader->sequence   =   $procHeader->sequence + 1;
                $procHeader->leader_name    =   $this->model['auth']->name;
                $procHeader->save();
                $procComment        =   new ProcComment();
                $procComment->name  =   $this->model['auth']->name;
                $procComment->role  =   $this->model['auth']->roleName;
                $procComment->process   =   $admProcess->name;
                $procComment->response  =   $request->get('response');
                $procComment->proc_id   =   $procHeader->id;
                $procComment->save();
                return Redirect::to(route('home'))->with("success", "Data berhasil disimpan");
            } else if ($request->get("submitType") == 'revision') {
                $procHeader     =   ProcHeader::find($request->get('id'));
                $admHirearchy   =   AdmHirearchy::where('sequence', ($procHeader->sequence - 1))
                                                    ->first();
                $admProcess     =   AdmProcess::find($procHeader->process);
                if (!$admHirearchy) {
                    return Redirect::to(route('home'))->with("error", "Proses tidak bisa dilanjutkan");
                }
                $procHeader->current    =   $admHirearchy->role;
                $procHeader->process    =   $admHirearchy->process;
                $procHeader->sequence   =   $procHeader->sequence - 1;
                $procHeader->save();
                $procComment        =   new ProcComment();
                $procComment->name  =   $this->model['auth']->name;
                $procComment->role  =   $this->model['auth']->roleName;
                $procComment->process   =   $admProcess->name;
                $procComment->response  =   $request->get('response');
                $procComment->proc_id   =   $procHeader->id;
                $procComment->save();
                return Redirect::to(route('home'))->with("success", "Data berhasil disimpan");
            } else if ($request->get("submitType") == 'reject') {
                $procHeader     =   ProcHeader::find($request->get('id'));
                $admHirearchy   =   AdmHirearchy::where('sequence', (1))
                                                    ->first();
                $admProcess     =   AdmProcess::find($procHeader->process);
                if (!$admHirearchy) {
                    return Redirect::to(route('home'))->with("error", "Proses tidak bisa dilanjutkan");
                }
                $procHeader->current    =   $admHirearchy->role;
                $procHeader->process    =   6;
                $procHeader->sequence   =   0;
                $procHeader->status     =   0;
                $procHeader->save();
                $procComment        =   new ProcComment();
                $procComment->name  =   $this->model['auth']->name;
                $procComment->role  =   $this->model['auth']->roleName;
                $procComment->process   =   $admProcess->name;
                $procComment->response  =   $request->get('response');
                $procComment->proc_id   =   $procHeader->id;
                $procComment->save();
                return Redirect::to(route('home'))->with("success", "Data berhasil disimpan");
            }
            return Redirect::to(route("home"));
        }
    
    // Form Upload Report Proc
        public function uploadReportProc (Request $request) {
            $this->readyModel();
            if ($request->get('submitType') == 'view') {
                $this->model['procHeader']  =   ProcHeader::find($request->get('id'));
                $this->model['token']       =   md5(date("d-M-Y H:i:s") . $this->model['procHeader']->id);
                $this->model['siteTitle']   =   'Pemeriksaan Formulir dan Unggah Draf Surat';
                return view('proc.formUploadReport', $this->model);
            } else if ($request->get("submitType") == 'process') {
                $procHeader     =   ProcHeader::find($request->get('id'));
                $admHirearchy   =   AdmHirearchy::where('sequence', ($procHeader->sequence + 1))
                                                    ->first();
                $admProcess     =   AdmProcess::find($procHeader->process);
                if (!$admHirearchy) {
                    return Redirect::to(route('home'))->with("error", "Proses tidak bisa dilanjutkan");
                }
                $procHeader->code_report        =   $request->get("code_report");
                $procHeader->final_doc_report   =   $request->get("final_doc_report");
                $procHeader->final_doc_number   =   $request->get("final_doc_number");
                $procHeader->current    =   $admHirearchy->role;
                $procHeader->process    =   $admHirearchy->process;
                $procHeader->sequence   =   $procHeader->sequence + 1;
                $procHeader->operator          =   $this->model['auth']->id;
                $procHeader->operator_name     =   $this->model['auth']->name;
                $procHeader->save();
                $procComment        =   new ProcComment();
                $procComment->name  =   $this->model['auth']->name;
                $procComment->role  =   $this->model['auth']->roleName;
                $procComment->process   =   $admProcess->name;
                $procComment->response  =   $request->get('response');
                $procComment->proc_id   =   $procHeader->id;
                $procComment->save();
                return Redirect::to(route('home'))->with("success", "Data berhasil disimpan");
            } else if ($request->get("submitType") == 'revision') {
                $procHeader     =   ProcHeader::find($request->get('id'));
                $admHirearchy   =   AdmHirearchy::where('sequence', ($procHeader->sequence - 1))
                                                    ->first();
                $admProcess     =   AdmProcess::find($procHeader->process);
                if (!$admHirearchy) {
                    return Redirect::to(route('home'))->with("error", "Proses tidak bisa dilanjutkan");
                }
                $procHeader->current    =   $admHirearchy->role;
                $procHeader->process    =   $admHirearchy->process;
                $procHeader->sequence   =   $procHeader->sequence - 1;
                $procHeader->save();
                $procComment        =   new ProcComment();
                $procComment->name  =   $this->model['auth']->name;
                $procComment->role  =   $this->model['auth']->roleName;
                $procComment->process   =   $admProcess->name;
                $procComment->response  =   $request->get('response');
                $procComment->proc_id   =   $procHeader->id;
                $procComment->save();
                return Redirect::to(route('home'))->with("success", "Data berhasil disimpan");
            } else if ($request->get("submitType") == 'reject') {
                $procHeader     =   ProcHeader::find($request->get('id'));
                $admHirearchy   =   AdmHirearchy::where('sequence', (1))
                                                    ->first();
                $admProcess     =   AdmProcess::find($procHeader->process);
                if (!$admHirearchy) {
                    return Redirect::to(route('home'))->with("error", "Proses tidak bisa dilanjutkan");
                }
                $procHeader->current    =   $admHirearchy->role;
                $procHeader->process    =   6;
                $procHeader->sequence   =   0;
                $procHeader->status     =   0;
                $procHeader->save();
                $procComment        =   new ProcComment();
                $procComment->name  =   $this->model['auth']->name;
                $procComment->role  =   $this->model['auth']->roleName;
                $procComment->process   =   $admProcess->name;
                $procComment->response  =   $request->get('response');
                $procComment->proc_id   =   $procHeader->id;
                $procComment->save();
                return Redirect::to(route('home'))->with("success", "Data berhasil disimpan");
            }
            return Redirect::to(route("home"));
        }

}
