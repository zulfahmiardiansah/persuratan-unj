<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ProcHeaderReport implements FromView
{
    public $listProcHeader;

    public function view(): View
    {
        $data["listProcHeader"] =   $this->listProcHeader;
        return view('excel.proc-excel', $data);
    }
}