@include('include/header')

<div class="my-3 my-md-5">
    <div class="container">
        <div class="page-header">
            <h1 class="page-title">
                Profile
            </h1>
        </div>
        <div class="row row-cards">
            <div class="col-12">
                <form action="" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ $admUser->id }}">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                {{ isset($admUser->id) ? 'Ubah' : 'Tambah' }} Data
                            </h3>
                        </div>
                        <div class="card-body">
                            <div class="row mb-3">
                                <div class="col-lg-2 col-12 custom-label">
                                    <label>
                                        Nama <sup class="mandatory-sup">*</sup>
                                    </label>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <input name="name" type="text" class="form-control" required="" value="{{ $admUser->name }}">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-lg-2 col-12 custom-label">
                                    <label>
                                        Email
                                    </label>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <input name="email" type="email" class="form-control" value="{{ $admUser->email }}">
                                </div>
                            </div>
                            <!-- <div class="row mb-3">
                                <div class="col-lg-2 col-12 custom-label">
                                    <label>
                                        Tanda Tangan 
                                        @if(!isset($admUser->id))
                                            <sup class="mandatory-sup">*</sup>
                                        @endif
                                    </label>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <input name="sign" accept="image/*" type="file" class="form-control" {{ isset($admUser->id) ? '' : 'required' }}>
                                    @if ($admUser->sign)
                                        <img src="{{ url($admUser->sign) }}" class="img-thumbnail" style="max-width: 200px; margin-top: 10px; width: 100%;">
                                    @endif
                                </div>
                            </div> -->
                            <div class="row">
                                <div class="col-lg-2 col-12 custom-label">
                                    <label>
                                        Kata Sandi  
                                        @if(!isset($admUser->id))
                                            <sup class="mandatory-sup">*</sup>
                                        @endif
                                    </label>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <input name="password" type="password" class="form-control" {{ isset($admUser->id) ? '' : 'required' }}>
                                    @if(isset($admUser->id))
                                        <span class="form-hint text-muted">
                                            * Isi jika ingin mengubah kata sandi
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-6 text-left">
                                    <a href="{{ route('home') }}">
                                        <button type="button" class="btn btn-danger">
                                            <i class="fe fe-home"></i>&nbsp; Beranda
                                        </button>
                                    </a>
                                </div>
                                <div class="col-6 text-right">
                                    <a href="{{ route('home') }}">
                                        <button name="submitType" value="save" class="btn btn-primary">
                                            <i class="fe fe-save"></i>&nbsp; Simpan
                                        </button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@include('include/footer')

<script>
    $('a[data-url-title="home"]').addClass("active");
</script>