@include('include/header')

<div class="my-3 my-md-5">

    <div class="container">
        <div class="page-header">
            <h1 class="page-title">
                Jenis
            </h1>
        </div>
        <div class="row row-cards">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <form action="" method="POST">
                            {{ csrf_field() }}
                            <button name="submitType" value="view" class="btn btn-sm btn-success">
                                <i class="fe fe-plus"></i> Tambah Data
                            </button>
                        </form>
                    </div>
                    <div class="table-responsive">
                        <table class="table card-table table-hover table-vcenter text-nowrap">
                            <thead>
                                <tr>
                                    <th class="w-4">No.</th>
                                    <th>Nama</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($listAdmType as $idx => $admType)
                                    <tr>
                                        <td>
                                            <span class="text-muted text-center">
                                                {{ $idx + 1 }}
                                            </span>
                                        </td>
                                        <td>
                                            {{ $admType->name }}
                                        </td>
                                        <td>
                                            <form action="" method="POST">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="id" value="{{ $admType->id }}">
                                                <button name="submitType" value="view" class="icon">
                                                    <i class="fe fe-edit"></i>
                                                </button>
                                                &nbsp;
                                                <button name="submitType" value="delete" class="icon">
                                                    <i class="fe fe-trash"></i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                @if (count($listAdmType) == 0)
                                    <tr>
                                        <td colspan="3" class="text-muted py-3">
                                            Tidak ada data
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('include/footer')

<script>
    $('a[data-url-title="master"]').addClass("active");
</script>