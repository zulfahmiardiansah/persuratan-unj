<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Redirect;

use App\Model\ProcHeader;
use App\Model\AdmAccompanient;
use App\Model\AdmPlace;
use App\Model\AdmType;
use App\Model\AdmRole;

use App\Exports\ProcHeaderReport;

class ReportController extends Controller
{
    // Model
        private $model  =   [];

    // Ready Model
        function readyModel () {
            $this->model    =   Utility::getSessionScope();
        }

    // Devotion Page
        public function devotionPage (Request $request)
        {
            $this->readyModel();
            $getPath    =   "";
            $pathArray  =   [];
            $this->model['siteTitle']       =   "Pengabdian";
            $this->model['listProcHeader']  =   ProcHeader::whereNull("is_delete")->where('type', 1)->orderBy("created_at", "DESC");
            if ($request->get("keyword")) {
                $this->model['listProcHeader']  =   $this->model['listProcHeader']->where(function ($q) use ($request) {
                                                            $q->where("title", "LIKE", "%" . $request->get("keyword") . "%")
                                                            ->orWhere("purpose", "LIKE", "%" . $request->get("keyword") . "%")
                                                            ->orWhere("subject", "LIKE", "%" . $request->get("keyword") . "%")
                                                            ->orWhere("number_report", "LIKE", "%" . $request->get("keyword") . "%")
                                                            ->orWhere("initiator_name", "LIKE", "%" . $request->get("keyword") . "%")
                                                            ->orWhere("operator_name", "LIKE", "%" . $request->get("keyword") . "%");
                                                        });
                $pathArray[]    =   "keyword=" . $request->get("keyword");
            }
            if ($request->get("from"))  {
                $this->model['listProcHeader']  =   $this->model['listProcHeader']->whereDate('created_at', '>=', $request->get('from'));
                $pathArray[]    =   "from=" . $request->get("from");
            }
            if ($request->get("to"))  {
                $this->model['listProcHeader']  =   $this->model['listProcHeader']->whereDate('created_at', '<=', $request->get('to'));
                $pathArray[]    =   "to=" . $request->get("to");
            }
            if (isset($this->model['auth'])) {
                if ($this->model['auth']->roleCode  ==  'ROLE_USER') {
                    $this->model['listProcHeader']  =   $this->model['listProcHeader']->where('initiator', $this->model['auth']->id);
                }
            }
            if (count($pathArray)) {
                $getPath    =   "?" . implode("&", $pathArray);
                $this->model["getPath"] =   $getPath;
            }
            $this->model['listProcHeader']  =   $this->model['listProcHeader']->paginate(10)->withPath($getPath);
            return view('proc.listRequest', $this->model);
        }

    // Research Page
        public function researchPage (Request $request)
        {
            $this->readyModel();
            $getPath    =   "";
            $pathArray  =   [];
            $this->model['siteTitle']       =   "Penelitian";
            $this->model['listProcHeader']  =   ProcHeader::whereNull("is_delete")->where('type', 2)->orderBy("created_at", "DESC");
            if ($request->get("keyword")) {
                $this->model['listProcHeader']  =   $this->model['listProcHeader']->where(function ($q) use ($request) {
                                                            $q->where("title", "LIKE", "%" . $request->get("keyword") . "%")
                                                            ->orWhere("purpose", "LIKE", "%" . $request->get("keyword") . "%")
                                                            ->orWhere("subject", "LIKE", "%" . $request->get("keyword") . "%")
                                                            ->orWhere("number_report", "LIKE", "%" . $request->get("keyword") . "%")
                                                            ->orWhere("initiator_name", "LIKE", "%" . $request->get("keyword") . "%")
                                                            ->orWhere("operator_name", "LIKE", "%" . $request->get("keyword") . "%");
                                                        });
                $pathArray[]    =   "keyword=" . $request->get("keyword");
            }
            if ($request->get("from"))  {
                $this->model['listProcHeader']  =   $this->model['listProcHeader']->whereDate('created_at', '>=', $request->get('from'));
                $pathArray[]    =   "from=" . $request->get("from");
            }
            if ($request->get("to"))  {
                $this->model['listProcHeader']  =   $this->model['listProcHeader']->whereDate('created_at', '<=', $request->get('to'));
                $pathArray[]    =   "to=" . $request->get("to");
            }
            if (isset($this->model['auth'])) {
                if ($this->model['auth']->roleCode  ==  'ROLE_USER') {
                    $this->model['listProcHeader']  =   $this->model['listProcHeader']->where('initiator', $this->model['auth']->id);
                }
            }
            if (count($pathArray)) {
                $getPath    =   "?" . implode("&", $pathArray);
                $this->model["getPath"] =   $getPath;
            }
            $this->model['listProcHeader']  =   $this->model['listProcHeader']->paginate(10)->withPath($getPath);
            return view('proc.listRequest', $this->model);
        }

    // View Page
        public function viewPage (Request $request) {
            $this->readyModel();
            $this->model['siteTitle']   =   'Informasi';
            $this->model['procHeader']  =   ProcHeader::find($request->get('id'));
            return view('proc.viewRequest', $this->model);
        }
        
    // Scan Page
        public function scanPage (Request $request, $code) {
            $this->readyModel();
            $this->model['siteTitle']   =   'Informasi Dokumen';
            $this->model['procHeader']  =   ProcHeader::where("code_report", $code)->first();;
            return view('proc.viewScan', $this->model);
        }
}
