-- MySQL dump 10.13  Distrib 5.5.62, for Win64 (AMD64)
--
-- Host: localhost    Database: silamabas
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.38-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `adm_accompanient`
--

DROP TABLE IF EXISTS `adm_accompanient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adm_accompanient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `adm_accompanient`
--

LOCK TABLES `adm_accompanient` WRITE;
/*!40000 ALTER TABLE `adm_accompanient` DISABLE KEYS */;
INSERT INTO `adm_accompanient` VALUES (2,'Pemeriksaan Awal',NULL,NULL),(3,'Pelimpahan Perkara',NULL,NULL),(4,'Persidangan',NULL,NULL),(5,'Pelaksanaan Putusan',NULL,NULL),(6,'Proses Diversi',NULL,NULL);
/*!40000 ALTER TABLE `adm_accompanient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `adm_hirearchy`
--

DROP TABLE IF EXISTS `adm_hirearchy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adm_hirearchy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` int(11) DEFAULT NULL,
  `process` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `proc_type` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `adm_hirearchy`
--

LOCK TABLES `adm_hirearchy` WRITE;
/*!40000 ALTER TABLE `adm_hirearchy` DISABLE KEYS */;
INSERT INTO `adm_hirearchy` VALUES (4,5,1,1,1,'2020-07-18 01:04:11','2020-07-18 01:04:11'),(5,3,2,2,1,'2020-07-18 01:04:17','2020-07-18 01:04:17'),(6,2,3,3,1,'2020-07-18 01:04:23','2020-07-18 01:04:23'),(7,4,4,4,1,'2020-07-18 23:13:28','2020-07-18 23:13:28'),(8,4,5,5,1,'2020-07-20 06:35:29','2020-07-20 06:35:29'),(9,2,6,6,1,'2020-07-20 07:32:39','2020-07-20 07:32:39'),(10,3,6,7,1,'2020-07-20 07:32:45','2020-07-20 07:32:45'),(11,5,7,8,1,'2020-07-20 07:51:04','2020-07-20 07:51:04'),(12,5,1,1,2,'2020-07-18 01:04:11','2020-07-18 01:04:11'),(13,3,2,2,2,'2020-07-18 01:04:17','2020-07-18 01:04:17'),(14,2,3,3,2,'2020-07-18 01:04:23','2020-07-18 01:04:23'),(15,4,4,4,2,'2020-07-18 23:13:28','2020-07-18 23:13:28'),(16,4,5,5,2,'2020-07-20 06:35:29','2020-07-20 06:35:29'),(17,2,6,6,2,'2020-07-20 07:32:39','2020-07-20 07:32:39'),(18,3,6,7,2,'2020-07-20 07:32:45','2020-07-20 07:32:45'),(19,5,7,8,2,'2020-07-20 07:51:04','2020-07-20 07:51:04');
/*!40000 ALTER TABLE `adm_hirearchy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `adm_place`
--

DROP TABLE IF EXISTS `adm_place`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adm_place` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `adm_place`
--

LOCK TABLES `adm_place` WRITE;
/*!40000 ALTER TABLE `adm_place` DISABLE KEYS */;
INSERT INTO `adm_place` VALUES (1,'LAPAS DKI Jakarta','2020-07-18 08:52:28','2020-07-18 08:52:28');
/*!40000 ALTER TABLE `adm_place` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `adm_process`
--

DROP TABLE IF EXISTS `adm_process`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adm_process` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `adm_process`
--

LOCK TABLES `adm_process` WRITE;
/*!40000 ALTER TABLE `adm_process` DISABLE KEYS */;
INSERT INTO `adm_process` VALUES (1,'Pengisian Form',NULL,NULL,NULL),(2,'Persetujuan','proc/approval',NULL,NULL),(3,'Persetujuan dan Pemberian Jangka Waktu','proc/approvalDuration',NULL,NULL),(4,'Unduh Dokumen Permintaan','proc/downloadDoc',NULL,NULL),(5,'Pengisian Laporan','proc/uploadReport',NULL,NULL),(6,'Persetujuan Laporan','proc/approvalReport',NULL,NULL),(7,'Unduh Laporan','proc/viewReport',NULL,NULL);
/*!40000 ALTER TABLE `adm_process` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `adm_role`
--

DROP TABLE IF EXISTS `adm_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adm_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `adm_role`
--

LOCK TABLES `adm_role` WRITE;
/*!40000 ALTER TABLE `adm_role` DISABLE KEYS */;
INSERT INTO `adm_role` VALUES (1,'Pengelola','ROLE_ADMIN',NULL,NULL),(2,'Kepala Subdirektorat','ROLE_KASUBDIT','2020-07-17 22:22:05','2020-07-17 22:22:05'),(3,'Kepala','ROLE_KEPALA','2020-07-17 22:22:37','2020-07-17 22:22:37'),(4,'Pembimbing Kemasyarakatan','ROLE_PK','2020-07-17 22:23:31','2020-07-17 22:23:31'),(5,'Pengguna','ROLE_USER','2020-07-17 23:12:18','2020-07-17 23:12:18');
/*!40000 ALTER TABLE `adm_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `adm_type`
--

DROP TABLE IF EXISTS `adm_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adm_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `adm_type`
--

LOCK TABLES `adm_type` WRITE;
/*!40000 ALTER TABLE `adm_type` DISABLE KEYS */;
INSERT INTO `adm_type` VALUES (1,'Pembebasan Bersyarat (PB)',NULL,NULL),(2,'Cuti Bersyarat (CB)',NULL,NULL),(3,'Cuti Menjelang Bebas (CMB)',NULL,NULL),(4,'Cuti Mengunjungi Keluarga (CMK)',NULL,NULL),(5,'Asimilasi',NULL,NULL),(6,'Pidana Dengan Syarat',NULL,NULL);
/*!40000 ALTER TABLE `adm_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `adm_user`
--

DROP TABLE IF EXISTS `adm_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adm_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `role` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sign` varchar(255) DEFAULT NULL,
  `nip` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `adm_user`
--

LOCK TABLES `adm_user` WRITE;
/*!40000 ALTER TABLE `adm_user` DISABLE KEYS */;
INSERT INTO `adm_user` VALUES (1,'admin','$2y$10$G/Ul0Dn7N.iswuqSmkmtZeCL4aeOIu6WEaNQ.9Yacsq39qWmBsExa','admin@silamabas.id','Pengelola',1,NULL,'2020-07-20 17:13:02',NULL,'01010101010'),(2,'pengguna','$2y$10$Lk6YzxJCQzuwhiJw6RjpUeRu/M.nFA.EKWyq.7TmNV7X6hxJAMRIu','pengguna@silamabas.id','Pengguna',5,'2020-07-18 00:19:49','2020-07-18 00:19:49',NULL,'01010101010'),(3,'kepala','$2y$10$4I7O1IDZaQ8a.77FMLprc.XOMcwf6fUgjTsYRdUjFvGUW29CAeoAa','kepala@silamabas.id','Kepala',3,'2020-07-18 03:45:46','2020-07-24 13:56:05','storage/files/media/2020-07-24/205605_Picture2.png','01010101010'),(4,'kasubdit','$2y$10$YCW/bWWyMzS44YsULUO/NeoxvkYWCqj1XpviamNHVaL.Qtx73NT26','kasubdit@silamabas.id','Kasubdit',2,'2020-07-18 10:34:10','2020-07-18 10:34:10',NULL,'01010101010'),(5,'pembimbing','$2y$10$PQF4wHb1p5lNkoC7gTV8je6KIZqdLnZBBvdUFyRpiZn2PKt3PVu0S','pembimbing@silamabas.id','Pembimbing Kemasyarakatan',4,'2020-07-18 23:19:17','2020-07-24 13:27:37','storage/files/media/2020-07-24/202737_signature-letter-j-opengl.png','01010101010');
/*!40000 ALTER TABLE `adm_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proc_accompanient`
--

DROP TABLE IF EXISTS `proc_accompanient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proc_accompanient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accompanient` int(11) DEFAULT NULL,
  `origin_agency` varchar(255) DEFAULT NULL,
  `abh_initial` varchar(255) DEFAULT NULL,
  `abh_address` text,
  `trustee_name` varchar(255) DEFAULT NULL,
  `trustee_phone` varchar(255) DEFAULT NULL,
  `investigator_name` varchar(255) DEFAULT NULL,
  `investigator_phone` varchar(255) DEFAULT NULL,
  `proc_id` int(11) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `prosecutor_name` varchar(255) DEFAULT NULL,
  `judge_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proc_accompanient`
--

LOCK TABLES `proc_accompanient` WRITE;
/*!40000 ALTER TABLE `proc_accompanient` DISABLE KEYS */;
/*!40000 ALTER TABLE `proc_accompanient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proc_client_acc`
--

DROP TABLE IF EXISTS `proc_client_acc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proc_client_acc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `program` int(11) DEFAULT NULL,
  `origin_upt` int(11) DEFAULT NULL,
  `wbp_name` varchar(255) DEFAULT NULL,
  `wbp_reg_number` varchar(255) DEFAULT NULL,
  `penalty` varchar(255) DEFAULT NULL,
  `decision_number` varchar(255) DEFAULT NULL,
  `decision_date` date DEFAULT NULL,
  `acceptance_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `proc_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proc_client_acc`
--

LOCK TABLES `proc_client_acc` WRITE;
/*!40000 ALTER TABLE `proc_client_acc` DISABLE KEYS */;
/*!40000 ALTER TABLE `proc_client_acc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proc_comment`
--

DROP TABLE IF EXISTS `proc_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proc_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `process` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `response` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `proc_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proc_comment`
--

LOCK TABLES `proc_comment` WRITE;
/*!40000 ALTER TABLE `proc_comment` DISABLE KEYS */;
INSERT INTO `proc_comment` VALUES (1,'Pengguna','Pengisian Form','Pengguna',NULL,'2020-07-24 09:50:59','2020-07-24 09:50:59',1),(2,'Kepala','Persetujuan','Kepala','-','2020-07-24 10:01:01','2020-07-24 10:01:01',1),(3,'Kasubdit','Persetujuan dan Pemberian Jangka Waktu','Kepala Subdirektorat','-','2020-07-24 10:11:09','2020-07-24 10:11:09',1),(4,'Pembimbing Kemasyarakatan','Unduh Dokumen Permintaan','Pembimbing Kemasyarakatan','-','2020-07-24 10:11:49','2020-07-24 10:11:49',1),(5,'Pembimbing Kemasyarakatan','Pengisian Laporan','Pembimbing Kemasyarakatan','-','2020-07-24 13:47:56','2020-07-24 13:47:56',1),(6,'Kasubdit','Persetujuan Laporan','Kepala Subdirektorat','-','2020-07-24 13:54:19','2020-07-24 13:54:19',1),(7,'Kepala','Persetujuan Laporan','Kepala','-','2020-07-24 14:09:59','2020-07-24 14:09:59',1);
/*!40000 ALTER TABLE `proc_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proc_header`
--

DROP TABLE IF EXISTS `proc_header`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proc_header` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `initiator` int(11) DEFAULT NULL,
  `form_type` int(11) DEFAULT NULL,
  `form_type_code` varchar(100) DEFAULT NULL,
  `proc_type` int(11) DEFAULT NULL,
  `clause` varchar(255) DEFAULT NULL,
  `case` varchar(255) DEFAULT NULL,
  `process` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `current` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `origin` varchar(255) DEFAULT NULL,
  `letter_number` varchar(255) DEFAULT NULL,
  `letter_date` date DEFAULT NULL,
  `start` date DEFAULT NULL,
  `end` date DEFAULT NULL,
  `report` varchar(255) DEFAULT NULL,
  `report_final` varchar(255) DEFAULT NULL,
  `worker` int(11) DEFAULT NULL,
  `report_signed` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proc_header`
--

LOCK TABLES `proc_header` WRITE;
/*!40000 ALTER TABLE `proc_header` DISABLE KEYS */;
INSERT INTO `proc_header` VALUES (1,2,1,'pre-adjudication',1,'Undang Undang','Penyalahgunaan Dana Bantuan Sosial PMI 2020',7,8,5,'2020-07-24 09:50:58','2020-07-24 14:09:59','Instansi','01-0023-BAPPAS','2020-07-01','2020-06-30','2020-07-30','storage/files/report/2020-07-24/204749_REPORT_1.pdf','storage/files/report/2020-07-24/210959_REPORT_FINAL_1.pdf',5,'storage/files/report/2020-07-24/204756_REPORT_SIGNED_1.pdf');
/*!40000 ALTER TABLE `proc_header` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proc_integration`
--

DROP TABLE IF EXISTS `proc_integration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proc_integration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `integration` int(11) DEFAULT NULL,
  `origin_upt` int(11) DEFAULT NULL,
  `wbp_name` varchar(255) DEFAULT NULL,
  `wbp_reg_number` varchar(255) DEFAULT NULL,
  `penalty` varchar(255) DEFAULT NULL,
  `phase` int(11) DEFAULT NULL,
  `trustee_name` varchar(255) DEFAULT NULL,
  `trustee_address` text,
  `trustee_relation` varchar(255) DEFAULT NULL,
  `trustee_phone` varchar(255) DEFAULT NULL,
  `proc_id` int(11) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proc_integration`
--

LOCK TABLES `proc_integration` WRITE;
/*!40000 ALTER TABLE `proc_integration` DISABLE KEYS */;
/*!40000 ALTER TABLE `proc_integration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proc_pre_adj`
--

DROP TABLE IF EXISTS `proc_pre_adj`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proc_pre_adj` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `origin_agency` varchar(255) DEFAULT NULL,
  `abh_initial` varchar(255) DEFAULT NULL,
  `abh_address` text,
  `trustee_name` varchar(255) DEFAULT NULL,
  `trustee_phone` varchar(255) DEFAULT NULL,
  `detention_status` int(11) DEFAULT NULL,
  `investigator_name` varchar(255) DEFAULT NULL,
  `investigator_phone` varchar(255) DEFAULT NULL,
  `proc_id` int(11) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proc_pre_adj`
--

LOCK TABLES `proc_pre_adj` WRITE;
/*!40000 ALTER TABLE `proc_pre_adj` DISABLE KEYS */;
INSERT INTO `proc_pre_adj` VALUES (1,'Instansi','ABH','Alamat ABH','Wali','0909899384912',1,'Penyidik','0238492381291',1,'storage/files/media/2020-07-24/165058_sample.pdf','2020-07-24 09:50:58','2020-07-24 09:50:58');
/*!40000 ALTER TABLE `proc_pre_adj` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proc_report`
--

DROP TABLE IF EXISTS `proc_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proc_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proc_id` int(11) DEFAULT NULL,
  `cover_subject` varchar(255) DEFAULT NULL,
  `cover_name` varchar(255) DEFAULT NULL,
  `cover_case` varchar(255) DEFAULT NULL,
  `cover_clause` varchar(255) DEFAULT NULL,
  `cover_address` varchar(255) DEFAULT NULL,
  `letter_number` varchar(255) DEFAULT NULL,
  `letter_atch` varchar(255) DEFAULT NULL,
  `letter_subject` varchar(255) DEFAULT NULL,
  `letter_doc` varchar(255) DEFAULT NULL,
  `letter_doc_total` varchar(255) DEFAULT NULL,
  `letter_doc_nb` text,
  `letter_copy` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proc_report`
--

LOCK TABLES `proc_report` WRITE;
/*!40000 ALTER TABLE `proc_report` DISABLE KEYS */;
INSERT INTO `proc_report` VALUES (1,1,'PENELITIAN KEMASYARAKATAN (LITMAS) UNTUK PROSES PERADILAN ANAK','Zulfahmi Ardiansah','Perkara A','Pasal A1','Gambir, Kecamatan Gambir, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta','W.12.BA.PK.01.05.02-','1 (Satu) Berkas','Hasil Penelitian Kemasyarakatan','Laporan Hasil Penelitian Kemasyarakatan (Litmas) atas nama :','01 (satu) berkas','Untuk memenuhi permintaan Litmas  Kepala Lapas Kelas IIA Serang. Demikian kami kirimkan untuk digunakan sebagaimana mestinya','Yth. Kepala Kantor Wilayah Kementerian Hukum dan HAM Banten di – SERANG','2020-07-24 13:47:48','2020-07-24 13:47:48');
/*!40000 ALTER TABLE `proc_report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ses_scope`
--

DROP TABLE IF EXISTS `ses_scope`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ses_scope` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) DEFAULT NULL,
  `value` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ses_scope`
--

LOCK TABLES `ses_scope` WRITE;
/*!40000 ALTER TABLE `ses_scope` DISABLE KEYS */;
INSERT INTO `ses_scope` VALUES (9,'$2y$10$iVTU3j9qT9NJvNfmfu/QRuTNFTULUzPf/ZFxG3.DVkirKyvukUkhW','{\"id\":2,\"name\":\"Pengguna\",\"roleCode\":\"ROLE_USER\",\"roleName\":\"Pengguna\"}','2020-07-24 15:09:08','2020-07-24 15:09:08');
/*!40000 ALTER TABLE `ses_scope` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'silamabas'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-24 22:46:25
