<?php

use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post("base64Upload", "Utility@base64Upload")->name("base64Upload");

Route::get("test", "ProcController@sample");

Route::get('/', 'HomeController@homePage')->name('home');
Route::get('/devotion', 'ReportController@devotionPage')->name('devotion');
Route::get('/research', 'ReportController@researchPage')->name('research');

Route::get('/login', 'AuthController@loginPage')->name('login');
Route::post('/login', 'AuthController@loginProc');
Route::get('/registration', 'AuthController@registrationPage')->name('registration');
Route::post('/registration', 'AuthController@registrationProc');

Route::get('/scan/{code}', 'ReportController@scanPage')->name("scan");

Route::middleware(["web", "login"])->group(function () {
    Route::get('/notification', 'HomeController@notificationProc')->name('notification');
    Route::get('/profile', 'AuthController@profilePage')->name('profile');
    Route::post('/profile', 'AuthController@profileProc');
    Route::get('/logout', 'AuthController@logoutProc')->name('logout');

    Route::get('/proc/form', 'ProcController@formPage')->name('proc/form');
    Route::post('/proc/form', 'ProcController@formSaveProc');
    Route::post('/proc/uploadDoc', 'ProcController@uploadDocProc');
    Route::post('/proc/approval', 'ProcController@approvalProc');
    Route::post('/proc/uploadReport', 'ProcController@uploadReportProc');

    Route::post('/report', 'ReportController@viewPage')->name('report');
});

Route::middleware(["web", "loginAdmin"])->group(function () {
    Route::get('/master/admUser', 'MasterController@admUserPage')->name('master/admUser');
    Route::post('/master/admUser', 'MasterController@admUserProc');

    Route::get('/master/admRole', 'MasterController@admRolePage')->name('master/admRole');
    Route::post('/master/admRole', 'MasterController@admRoleProc');

    Route::get('/master/admHirearchy', 'MasterController@admHirearchyPage')->name('master/admHirearchy');
    Route::post('/master/admHirearchy', 'MasterController@admHirearchyProc');
});