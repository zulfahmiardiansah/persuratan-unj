<meta charset="UTF-8">
<meta name="viewport"
    content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta http-equiv="Content-Language" content="en" />
<meta name="developer" content="xxdev.id">
<meta name="developer-site" content="https://zulfahmi-ardiansah.github.io/">
<meta name="msapplication-TileColor" content="#2d89ef">
<meta name="theme-color" content="#4188c9">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="mobile-web-app-capable" content="yes">
<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">
<link rel="icon" href="./favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" type="image/x-icon" href="{{ asset('./assets/images/brand/surat-unj.png') }}" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=latin-ext">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="//mozilla.github.io/pdf.js/build/pdf.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link href="{{ asset('./assets/css/toastr.min.css') }}" rel="stylesheet" />
<script src="{{ asset('./assets/js/toastr.min.js') }}"></script>
<script src="{{ asset('./assets/js/require.min.js') }}"></script>
<script>
    requirejs.config({
        baseUrl: '{{ route("home") }}'
    });
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
</script>
<link href="{{ asset('./assets/css/dashboard.css') }}" rel="stylesheet" />
<script src="{{ asset('./assets/js/dashboard.js') }}"></script>
<link href="{{ asset('./assets/plugins/charts-c3/plugin.css') }}" rel="stylesheet" />
<script src="{{ asset('./assets/plugins/charts-c3/plugin.js') }}"></script>
<link href="{{ asset('./assets/plugins/maps-google/plugin.css') }}" rel="stylesheet" />
<script src="{{ asset('./assets/plugins/maps-google/plugin.js') }}"></script>
<script src="{{ asset('./assets/plugins/input-mask/plugin.js') }}"></script>