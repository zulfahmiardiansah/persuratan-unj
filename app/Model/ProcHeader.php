<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProcHeader extends Model
{
    // Definition
        protected $table = "proc_header";

    // Join
        public function currentRel ()
        {
            return $this->belongsTo(AdmRole::class, 'current','id');
        }

        public function processRel ()
        {
            return $this->belongsTo(AdmProcess::class, 'process','id');
        }

        public function initiatorRel ()
        {
            return $this->belongsTo(AdmUser::class, 'initiator','id');
        }

        public function operatorRel ()
        {
            return $this->belongsTo(AdmUser::class, 'operator','id');
        }

        public function commentRel () {
            return ProcComment::where("proc_id", $this->id)->orderBy("id", "ASC")->get();
        }
}
