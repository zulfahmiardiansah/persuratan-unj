@include('include/header')

<div class="my-3 my-md-5">

    <div class="container">
        <div class="page-header">
            <h1 class="page-title">
                Informasi Dokumen
            </h1>
        </div>
        <div class="row row-cards">
            <div class="col-12">
                <div class="card">
                    <div class="card-header py-5">
                        <img src="{{ asset('./assets/images/brand/unj.png') }}" style="width: 200px; margin: auto; max-width: 100%; display: block;">
                    </div>
                    <div class="card-body">
                        <div class="row mb-3">
                            <div class="col-lg-3 col-12 custom-label">
                                <label>
                                    Nomor Surat
                                </label>
                            </div>
                            <div class="col-lg-6 col-12">
                                <div style="padding-top: 6px;">
                                    {{ $procHeader->final_doc_number }}
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-3 col-12 custom-label">
                                <label>
                                    Judul
                                </label>
                            </div>
                            <div class="col-lg-6 col-12">
                                <div style="padding-top: 6px;">
                                    {{ $procHeader->title }}
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-3 col-12 custom-label">
                                <label>
                                    Jenis
                                </label>
                            </div>
                            <div class="col-lg-6 col-12">
                                <div style="padding-top: 6px;">
                                    @if ($procHeader->type == 1)
                                        Pengabdian
                                    @else
                                        Penelitian
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-3 col-12 custom-label">
                                <label>
                                    Perihal
                                </label>
                            </div>
                            <div class="col-lg-6 col-12">
                                <div style="padding-top: 6px;">
                                    {{ $procHeader->purpose }}
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-3 col-12 custom-label">
                                <label>
                                    Tujuan
                                </label>
                            </div>
                            <div class="col-lg-6 col-12">
                                <div style="padding-top: 6px;">
                                    @if (count(explode("|", $procHeader->subject)) == 1)
                                        {{ str_replace("|", "", $procHeader->subject) }}
                                    @else
                                        <ul style="padding-left: 15px; margin-bottom: 0px;">
                                            @foreach(explode("|", $procHeader->subject) as $subject)
                                                <li>
                                                    {{ $subject }}
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-3 col-12 custom-label">
                                <label>
                                    Dosen
                                </label>
                            </div>
                            <div class="col-lg-6 col-12">
                                <div style="padding-top: 6px;">
                                    {{ $procHeader->initiator_name }}
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-3 col-12 custom-label">
                                <label>
                                    Ditandatangani Oleh
                                </label>
                            </div>
                            <div class="col-lg-6 col-12">
                                <div style="padding-top: 6px;">
                                    Ketua LPPM Universitas Negeri Jakarta
                                    <small class="d-block">{{ $procHeader->leader_name }}</small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-3 col-12 custom-label">
                                <label>
                                    Surat
                                </label>
                            </div>
                            <div class="col-lg-6 col-12">
                                <div style="padding-top: 6px;">
                                    <a href="{{ url($procHeader->final_doc_report) }}" download class="btn btn-sm btn-secondary">
                                        <i class="fe fe-download"></i>&nbsp; Unduh
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('include/footer')

<script>
    $('a[data-url-title="{{ $procHeader->type == 1 ? 'pengabdian' : 'penelitian' }}"]').addClass("active");
</script>

<style>
    .custom-label label {
        color: #999;
    }
</style>