@include('include/header')

<div class="my-3 my-md-5">

    <div class="container">
        <div class="page-header">
            <h1 class="page-title">
                Tempat
            </h1>
        </div>
        <div class="row row-cards">
            <div class="col-12">
                <form action="" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ $admPlace->id }}">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                {{ isset($admPlace->id) ? 'Ubah' : 'Tambah' }} Data
                            </h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-2 col-12 custom-label">
                                    <label>
                                        Nama <sup class="mandatory-sup">*</sup>
                                    </label>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <input name="name" type="text" class="form-control" required="" value="{{ $admPlace->name }}">
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-6">
                                    <a href="{{ route('master/admPlace') }}">
                                        <button type="button" class="btn btn-danger">
                                            <i class="fe fe-arrow-left"></i>&nbsp; Kembali
                                        </button>
                                    </a>
                                </div>
                                <div class="col-6 text-right">
                                    <button name="submitType" value="save" class="btn btn-primary">
                                        <i class="fe fe-save"></i>&nbsp; Simpan
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@include('include/footer')

<script>
    $('a[data-url-title="master"]').addClass("active");
</script>