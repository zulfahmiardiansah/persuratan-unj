<!doctype html>
<html lang="en" dir="ltr">
<head>
    <title>Surat UNJ | Masuk</title>
    @include('include/asset')
</head>
<body>
    <div class="page">
        <div class="page-single">
            <div class="container">
                <div class="row">
                    <div class="col col-login mx-auto">
                        <div class="text-center mb-5">
                            <img src="{{ asset('./assets/images/brand/unj.png') }}" style="height: 165px;" alt="">
                        </div>
                        <form class="card" action="" method="post">
                            {{ csrf_field() }}
                            <div class="card-body p-6">
                                <div class="card-title">Masuk ke akun anda</div>
                                <div class="form-group">
                                    <label class="form-label">NID / NIDN</label>
                                    <input type="text" name="nid" required="" class="form-control" placeholder="">
                                </div>
                                <div class="form-group mb-0">
                                    <label class="form-label">
                                        Kata Sandi
                                    </label>
                                    <input type="password" name="password" required="" class="form-control" placeholder="">
                                </div>
                                <div class="form-footer mt-4">
                                    <button type="submit" class="btn btn-success btn-block">Masuk</button>
                                </div>
                            </div>
                        </form>
                        <div class="text-center text-muted">
                            <span>LPPM Universitas Negeri Jakarta</span> @ <b style="color: #00923f">{{ date("Y") }}</b>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(Session::has('error'))
        <script>
            toastr["error"]("{{ Session::get('error') }}", "Kesalahan")
        </script>
    @endif
    @if(Session::has('success'))
        <script>
            toastr["success"]("{{ Session::get('success') }}", "Pemberitahuan")
        </script>
    @endif
</body>
</html>