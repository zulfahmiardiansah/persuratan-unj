<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AdmUser extends Model
{
    // Definition
        protected $table    =   "adm_user";

    // Join
        public function roleRel ()
        {
            return $this->belongsTo(AdmRole::class, 'role','id');
        }
}
