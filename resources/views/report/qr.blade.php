<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>QR Pengesahan Dokumen</title>
    <style>
        body {
            text-align: center;
            font-size: 11pt;
        }

        table td {
            border: 1px solid black;
            padding: 4px 15px;
            text-align: left;
        }

        table {
            border-collapse: collapse;
            margin: auto;
        }

        .inf-td {
            width: 100px;
        }

        .inf-qr {
            width: 100px;
            text-align: center;
        }

        .inf-qr {
            width: 80px;
        }
    </style>
</head>

<body>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="4" class="inf-qr">
                <img src="https://api.qrserver.com/v1/create-qr-code/?size=135x135&data={{ route('scan', $procHeader->code_report) }}" alt="">
            </td>
            <td class="inf-td">
                <b>
                    Judul
                </b>
            </td>
            <td>
                {{  $procHeader->title }}
            </td>
        </tr>
        <tr>
            <td class="inf-td">
                <b>
                    Tanggal Permintaan
                </b>
            </td>
            <td>
                Pukul {{  date("H.i, d F Y", strtotime($procHeader->created_at)) }}
            </td>
        </tr>
        <tr>
            <td class="inf-td">
                <b>
                    Tanggal Selesai Diproses
                </b>
            </td>
            <td>
                Pukul {{  date("H.i, d F Y", strtotime($procHeader->updated_at)) }}
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Anda dapat memeriksa informasi dokumen ini dengan scan QR Code di samping.
            </td>
        </tr>
    </table>
</body>

</html>
