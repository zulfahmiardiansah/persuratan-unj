@include('include/header')

<div class="my-3 my-md-5">

    <div class="container">
        <div class="page-header">
            <h1 class="page-title">
                {{ $siteTitle }}
            </h1>
        </div>
        <form method="POST" action="" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="id" value="{{ $procHeader->id }}">
            <div class="row row-cards">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                Data
                            </h3>
                        </div>
                        <div class="card-body">
                            <div class="row mb-3">
                                <div class="col-lg-3 col-12 custom-label">
                                    <label>
                                        Jenis
                                    </label>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <div style="padding-top: 6px;">
                                        @if ($procHeader->type == 1)
                                            Pengabdian
                                        @else
                                            Penelitian
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-lg-3 col-12 custom-label">
                                    <label>
                                        Tanggal Permintaan
                                    </label>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <div style="padding-top: 6px;">
                                        {{ date('d M Y, H:i:s', strtotime($procHeader->created_at)) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-lg-3 col-12 custom-label">
                                    <label>
                                        Terakhir Diperbarui
                                    </label>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <div style="padding-top: 6px;">
                                        {{ date('d M Y, H:i:s', strtotime($procHeader->updated_at)) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3 col-12 custom-label">
                                    <label>
                                        Proses
                                    </label>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <div style="padding-top: 6px; display: flex;align-items: center;">
                                        <span class="status-icon bg-success"></span> 
                                        <div class="d-inline-block pl-2">
                                            {{ $procHeader->processRel->name }}
                                            <div class="small text-muted">
                                                {{ $procHeader->currentRel->name }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row mb-3">
                                <div class="col-lg-3 col-12 custom-label">
                                    <label>
                                        Judul
                                    </label>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <div style="padding-top: 6px;">
                                        {{ $procHeader->title }}
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-lg-3 col-12 custom-label">
                                    <label>
                                        Perihal
                                    </label>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <div style="padding-top: 6px;">
                                        {{ $procHeader->purpose }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3 col-12 custom-label">
                                    <label>
                                        Tujuan
                                    </label>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <div style="padding-top: 6px;">
                                        @if (count(explode("|", $procHeader->subject)) == 1)
                                            {{ str_replace("|", "", $procHeader->subject) }}
                                        @else
                                            <ul style="padding-left: 15px; margin-bottom: 0px;">
                                                @foreach(explode("|", $procHeader->subject) as $subject)
                                                    <li>
                                                        {{ $subject }}
                                                    </li>
                                                @endforeach
                                            </ul>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if ($procHeader->content_doc_letter || $procHeader->attachment_doc_letter)
                            <div class="card-body">
                                @if ($procHeader->content_doc_letter)
                                    <div class="row mb-3">
                                        <div class="col-lg-3 col-12 custom-label">
                                            <label>
                                                Isi Surat
                                            </label>
                                        </div>
                                        <div class="col-lg-6 col-12">
                                            <div style="padding-top: 6px;">
                                                <a href="{{ url($procHeader->content_doc_letter) }}" download class="btn btn-sm btn-secondary">
                                                    <i class="fe fe-download"></i>&nbsp; Unduh
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                @if ($procHeader->attachment_doc_letter)
                                    <div class="row">
                                        <div class="col-lg-3 col-12 custom-label">
                                            <label>
                                                Lampiran Surat
                                            </label>
                                        </div>
                                        <div class="col-lg-6 col-12">
                                            <div style="padding-top: 6px;">
                                                <a href="{{ url($procHeader->attachment_doc_letter) }}" download class="btn btn-sm btn-secondary">
                                                    <i class="fe fe-download"></i>&nbsp; Unduh
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        @endif
                        <div class="card-body">
                            <div class="row mb-3">
                                <div class="col-lg-3 col-12 custom-label">
                                    <label>
                                        Tanggal Awal Pelaksanaan
                                    </label>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <div style="padding-top: 6px;">
                                        {{ date('d M Y', strtotime($procHeader->start_date)) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3 col-12 custom-label">
                                    <label>
                                        Tanggal Selesai Pelaksanaan
                                    </label>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <div style="padding-top: 6px;">
                                        {{ date('d M Y', strtotime($procHeader->end_date)) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-3 col-12 custom-label">
                                    <label>
                                        Draf Surat 
                                        @if (!$procHeader->doc_report)
                                            <sup class="mandatory-sup">*</sup>
                                        @endif
                                    </label>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <div class="row gutters-xs">
                                        <div class="col">
                                            @if ($procHeader->doc_report)
                                                <input name="doc_report" type="file" class="form-control">
                                            @else 
                                                <input name="doc_report" type="file" required="" class="form-control">
                                            @endif
                                        </div>
                                        @if ($procHeader->doc_report)
                                            <span class="col-auto">
                                                <a href="{{ url($procHeader->doc_report) }}" download>
                                                    <button class="btn btn-secondary" type="button">
                                                        <i class="fe fe-download"></i> Berkas Sebelumnya
                                                    </button>
                                                </a>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                Catatan
                            </h3>
                        </div>
                        <div class="table-responsive">
                            <table class="table card-table table-hover table-vcenter text-nowrap">
                                <thead>
                                    <tr>
                                        <th class="w-1">No.</th>
                                        <th>Tanggal</th>
                                        <th>Proses</th>
                                        <th>Nama</th>
                                        <th>Role</th>
                                        <th>Catatan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($procHeader->commentRel() as $idx => $procComment)
                                        <tr>
                                            <td>
                                                {{ $idx + 1 }}.
                                            </td>
                                            <td>
                                                {{ date("d M Y, H:i:s", strtotime($procComment->created_at)) }}
                                            </td>
                                            <td>
                                                {{ $procComment->process }}
                                            </td>
                                            <td>
                                                {{ $procComment->name }}
                                            </td>
                                            <td>
                                                {{ $procComment->role }}
                                            </td>
                                            <td>
                                                {{ $procComment->response }}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                Catatan / Komentar
                            </h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-3 col-12 custom-label">
                                    <label>
                                        Catatan
                                    </label>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <textarea name="response" spellcheck="false" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <a href="{{ route('home') }}">
                        <button type="button" class="btn btn-md btn-secondary d-inline-block">
                            <i class="fe fe-arrow-left"></i> Kembali
                        </button>
                    </a>
                </div>
                <div class="col-6 text-right">
                    <button type="button" onclick="submitWithoutValidation('btn-reject')" class="btn btn-md btn-danger d-inline-block">
                        <i class="fe fe-edit"></i> Tolak
                    </button>
                    &nbsp;
                    <button type="button" onclick="submitWithoutValidation('btn-revision')" class="btn btn-md btn-warning d-inline-block">
                        <i class="fe fe-edit"></i> Revisi
                    </button>
                    &nbsp;
                    <button name="submitType" value="process" class="btn btn-md btn-success d-inline-block">
                        <i class="fe fe-save"></i> Setuju
                    </button>
                </div>
                <div class="d-none">
                    <button name="submitType" value="reject" class="btn-reject"></button>
                    <button name="submitType" value="revision" class="btn-revision"></button>
                </div>
            </div>
        </form>
    </div>
</div>

@include('include/footer')

<script>
    $('a[data-url-title="{{ $procHeader->type == 1 ? 'pengabdian' : 'penelitian' }}"]').addClass("active");
    $('form').submit(function(e){
        $('button').attr("type", "button")
    });
</script>

<style>
    .custom-label label {
        color: #999;
    }
</style>