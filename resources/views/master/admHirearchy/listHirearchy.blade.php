@include('include/header')

<div class="my-3 my-md-5">

    <div class="container">
        <div class="page-header">
            <h1 class="page-title">
                Hirarki
            </h1>
        </div>
        <div class="row row-cards">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            Daftar Proses
                        </h3>
                    </div>
                    <div class="card-body" style="border-bottom: 1px solid rgba(0, 40, 100, 0.12)">
                        <form action="" method="POST">
                            {{ csrf_field() }}
                            <div class="row mb-3">
                                <div class="col-lg-2 col-12 custom-label">
                                    <label>
                                        Proses <sup class="mandatory-sup">*</sup>
                                    </label>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <select name="process" class="form-control" required>
                                        <option value="">
                                            - Pilih Proses -
                                        </option>
                                        @foreach ($listAdmProcess as $admProcess)
                                            <option value="{{ $admProcess->id }}">
                                                {{ $admProcess->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-lg-2 col-12 custom-label">
                                    <label>
                                        Role <sup class="mandatory-sup">*</sup>
                                    </label>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <select name="role" class="form-control" required>
                                        <option value="">
                                            - Pilih Role -
                                        </option>
                                        @foreach ($listAdmRole as $admRole)
                                            <option value="{{ $admRole->id }}">
                                                {{ $admRole->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-lg-2 col-12 custom-label">
                                    <label>
                                        Sequence <sup class="mandatory-sup">*</sup>
                                    </label>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <input value="{{ count($listAdmHirearchyBKA) + 1 }}" readonly="" type="number" name="sequence" min="1" class="form-control" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 offset-lg-2 col-12">
                                    <button name="submitType" value="save" class="btn btn-success">
                                        <i class="fe fe-plus"></i>&nbsp; Tambah Data
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="table-responsive">
                        <table class="table card-table table-hover table-vcenter text-nowrap">
                            <thead>
                                <tr>
                                    <th class="w-8">Sequence</th>
                                    <th>Role</th>
                                    <th>Proses</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($listAdmHirearchyBKA as $idx => $admHirearchy)
                                    <tr>
                                        <td>
                                            <span class="text-muted text-center">
                                                {{ $admHirearchy->sequence }}
                                            </span>
                                        </td>
                                        <td>
                                            {{ $admHirearchy->roleRel->name }}
                                        </td>
                                        <td>
                                            {{ $admHirearchy->processRel->name }}
                                        </td>
                                        <td>
                                            <form action="" method="POST">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="id" value="{{ $admHirearchy->id }}">
                                                <button name="submitType" value="delete" class="icon">
                                                    <i class="fe fe-trash"></i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                @if (count($listAdmHirearchyBKA) == 0)
                                    <tr>
                                        <td colspan="3" class="text-muted py-3">
                                            Tidak ada data
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('include/footer')

<script>
    $('a[data-url-title="master"]').addClass("active");
</script>