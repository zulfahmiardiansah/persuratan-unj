@include('include/header')

<div class="my-3 my-md-5">

    <div class="container">
        <div class="page-header">
            <h1 class="page-title">
                Beranda
            </h1>
        </div>
        <div class="row row-cards">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title py-2">
                            Daftar Pekerjaan
                        </h3>
                    </div>
                    <div class="table-responsive">
                        <table class="table card-table table-hover table-vcenter text-nowrap">
                            <thead>
                                <tr>
                                    <th class="w-1">No. Surat</th>
                                    <th style="max-width: 250px;">Tanggal Pengajuan</th>
                                    <th>Jenis</th>
                                    <th>Judul</th>
                                    <th>Progress Penyelesaian</th>
                                    <th>
                                        {{ isset($auth) ? '' : 'Keterangan' }}
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (count($listProcHeader) == 0) 
                                    <tr>
                                        <td colspan="6">
                                            Tidak ada data
                                        </td>
                                    </tr>
                                @endif
                                @foreach ($listProcHeader as $idx => $procHeader)
                                    <tr>
                                        <td>
                                            <span class="text-muted">{{ $procHeader->number_report ? $procHeader->number_report : 'Belum memiliki nomor' }}</span>
                                        </td>
                                        <td>
                                            <a class="text-inherit">
                                                {{ date('d M Y', strtotime($procHeader->created_at)) }}
                                                <span class="d-md-inline-block d-none">
                                                    , {{ date('H:i:s', strtotime($procHeader->created_at)) }}
                                                </span>
                                            </a>
                                        </td>
                                        <td style="max-width: 250px; white-space: normal;">
                                            {{ $procHeader->type == 1 ? 'Pengabdian' : 'Penelitian' }}
                                        </td>
                                        <td style="max-width: 250px; white-space: normal;">
                                            {{ $procHeader->title }}
                                        </td>
                                        <td style="display: flex; align-items: center;">
                                            @if ($procHeader->status == 1) 
                                                <span class="status-icon bg-success mr-3"></span>
                                            @else
                                                <span class="status-icon bg-danger mr-3"></span>
                                            @endif
                                            <div class="d-inline-block">
                                                {{ $procHeader->processRel->name }}
                                                <div class="small text-muted">
                                                    {{ $procHeader->currentRel->name }}
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            @if (isset($auth))
                                                @if ($procHeader->final_doc_report)
                                                    <form action="{{ route('report') }}" method="POST">
                                                @else
                                                    <form action="{{ $procHeader->processRel->url }}" method="POST">
                                                @endif
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="id" value="{{ $procHeader->id }}">
                                                    <button name="submitType" value="view" class="btn btn-secondary btn-sm">
                                                        Buka
                                                    </button>
                                                </form>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('include/footer')

<script>
    $('a[data-url-title="home"]').addClass("active");
</script>
